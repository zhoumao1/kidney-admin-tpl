module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  extends: ['plugin:vue/recommended', 'eslint:recommended'],

  // add your custom rules here
  //it is base on https://github.com/vuejs/eslint-config-vue
  rules: {
    'no-useless-escape': 'off',
    'generator-star-spacing': 'off',
    // allow //debugger during development
    // 'no-//debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-//debugger': 'off',
    'vue/no-parsing-error': [2, { 'x-invalid-end-tag': false }],
    'no-undef': 'error',
    'no-console': 'off',
    'no-unused-vars': 'off',
    'no-constant-condition': 'off',
    'no-empty': 'off',
    'vue/html-indent': ['off'],
    'vue/singleline-html-element-content-newline': 'off',
    'vue/max-attributes-per-line': 'off',
    'vue/multiline-html-element-content-newline': 'off',
    'vue/html-closing-bracket-newline': 'off',
    'no-extra-boolean-cast': 'off',
    'vue/name-property-casing': 'off',
    'vue/prop-name-casing': 'off',
    'vue/require-default-prop': 'off',
    'vue/attributes-order': 'off',
    'vue/no-unused-vars': 'off',
    'no-prototype-builtins': 'off',
    'vue/attribute-hyphenation': 'off',
    'vue/html-closing-bracket-spacing': 'off',
  }
}
