//上传阿里云图库
import { httpget, httppost } from '@/libs/api-request';
import axios from 'axios'

export function GetOSSPolicyAndSignForMiniApp() {
	return httpget({
		url: '/api/MiniApp/GetOSSPolicyAndSignForMiniApp',
		params: {}
	});
}

//获取阿里云图库 http://doc.shensx.com/project/12/interface/api/1258
export function GeneratePresignedUri(data) {
	return httppost({
		url: '/api/WXH5Api/BatchGeneratePresignedUri',
		// showloading: true,
		data: {
			data: data
		}
	})
}

//获取阿里云图库 http://doc.shensx.com/project/12/interface/api/1258
export function SetOSSResourcePublicReadOnlyForWorker(data) {
	return httppost({
		url: '/micro/toolservice/microapi/OSSCommonApi/SetOSSResourcePublicReadOnlyForWorker',
		// showloading: true,
		data: { data }
	})
}

export const downloadFileByOss = async (file_osskey) => {
	try {
		let { data } = await GeneratePresignedUri([{
			osskey: file_osskey
		}])

		let url = process.env.NODE_ENV === 'production' ? data[0].url.replace(/http/, 'https') : data[0].url
		// let url = data[0].url
		// window.open(url, '_blank')
		// return Promise.resolve()
		const instance = axios.create({
			responseType: 'blob'
		})
		let { data: downloadUrl } = await instance.get(url)
		const blob = new Blob([downloadUrl], {
			type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;application/vnd.ms-excel;charset=utf-8'
		});
		let index = file_osskey.lastIndexOf('/') + 1
		let fileName = file_osskey.slice(index)
		if (
			'download' in document.createElement('a') &&
			navigator.userAgent.indexOf('Edge') <= -1
		) {
			// 非IE 及edge下载
			const elink = document.createElement('a');
			elink.download = fileName;
			elink.style.display = 'none';
			elink.href = URL.createObjectURL(blob);
			document.body.appendChild(elink);
			elink.click();
			URL.revokeObjectURL(elink.href); // 释放URL 对象
			document.body.removeChild(elink);
			return Promise.resolve()
		} else {
			// IE10+下载
			navigator.msSaveOrOpenBlob(blob, fileName);
		}
	} catch (e) {

		return Promise.reject(e)
	}

}
