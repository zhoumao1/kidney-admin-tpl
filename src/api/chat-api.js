import { httpget, httppost } from '@/libs/api-request';
//开启对话 http://doc.shensx.com/project/22/interface/api/2621
export function OpenDialog(data, is_cancel) {
	return httppost({
		url: '/micro/customerservice/microapi/CustomerServiceDialogApi/OpenDialog',
		data: { data },
		is_cancel
	})
}
//分页获取聊天消息 http://doc.shensx.com/project/22/interface/api/2631
export function GetDialogMessagePageListByLT(params) {
	return httpget({
		url: '/micro/customerservice/microapi/CustomerServiceDialogApi/GetDialogMessagePageListByLT',
		params
	})
}
//发送消息 http://doc.shensx.com/project/22/interface/api/2625
export function SendDialogMessage(data) {
	return httppost({
		url: '/micro/customerservice/microapi/CustomerServiceDialogApi/SendDialogMessage',
		data: { data }
	})
}

//分页获取上下文消息内容 http://doc.shensx.com/project/22/interface/api/3503
export function GetDialogMessagePageListByLTBeFree(params) {
	return httpget({
		url: '/micro/customerservice/microapi/CustomerServiceDialogApi/GetDialogMessagePageListByLTBeFree',
		params
	})
}
