//START
import {
	encrypt
} from '@/libs/security'
import { httpget, httppost } from '@/libs/api-request';

const qs = require('querystring')
//END

// 获取token
export const getAccessToken = ({ userName, password }) => {
	//  //debugger
	var url = '/api/UserInfoAPI/GetAccessToken'
	var authinfo = {
		userid: userName,
		password: password
	}
	var ut = encrypt(JSON.stringify(authinfo))
	// console.log(ut, 'ututututu')
	return httpget({
		url,
		request_info: {
			Accept: 'application/json',
			ContentType: 'application/json',
			UT: ut
		}
	})
}

//登陆康复师工作站--获得什么样的权限
export function LoginKidneyOnlineAPI(data) {
	return httppost({
		url: '/api/KYDepartmentApi/LoginKidneyOnlineForKYAdmin',
		data: { data: {} }
	})
}
