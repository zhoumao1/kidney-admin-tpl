import Velocity from './Velocity'
export class FLIP {
	constructor() {
		this.startElement = null
		this.endElement = null
		this.scrollTop = 0
		this.scrollLeft = 0
	}
	// 添加 transform-origin
	setTransOrigin(ele, is_clear = false) {
		ele.style.transformOrigin = is_clear ? '' : 'top left'
	}
	/**
	 * 过渡
	 * @param start_target  {string}    需要进行过渡的元素(传入css选择器)
	 * @param end_target    {string}    过渡后 展示的元素(传入css选择器)
	 */
	start(start_target, end_target) {

		const startElement = this.startElement = document.querySelector(start_target)
		const endElement = this.endElement = document.querySelector(end_target)

		startElement.setAttribute('data-kdy-start-ele', '')
		endElement.setAttribute('data-kdy-end-ele', '')
		this.setTransOrigin(startElement)

		endElement.style.display = 'block'
		startElement.style.visibility = 'hidden'

		endElement.addEventListener('click', () =>{
			this.onShrink(startElement, endElement)
		})
		this.onExpand(startElement, endElement)
	}

	/**
	 * 展开(放大) 处理
	 * @param firstEle   {Element}   起点元素
	 * @param lastEle   {Element}    终点元素
	 * @param onfinish   {function=}
	 */
	onExpand(firstEle, lastEle, onfinish) {
		let first = firstEle.getBoundingClientRect()
		let last = lastEle.getBoundingClientRect()

		let invertPosition = { x: first.left - last.left, y: first.top - last.top }
		let invertPX = { width: first.width / last.width, height: first.height / last.height }
		const parent = lastEle.parentElement
		this.scrollTop = parent.scrollTop
		this.scrollLeft = parent.scrollLeft
		if(!Velocity) return
		Velocity(lastEle, {
			// translateX: [终点, 起点]
			translateX: [0, invertPosition.x],
			translateY: [0, invertPosition.y],
			scaleX: [1, invertPX.width],
			scaleY: [1, invertPX.height]
		}, {
			duration: 460,
			easing: [.4, 0, .22, 1],
			progress: (e, complete) => {
				if(complete !== 1) {
					// this.zoomContainer.style.pointerEvents = 'none'
				}
			},
			complete: () => {
				// lastEle.offsetParent.style.overflow = 'hidden'
				// this.getImageEXIF(lastEle)
				// this.zoomContainer.style.pointerEvents = ''
				onfinish && onfinish()
			}
		})

	}

	/**
	 * 收缩(还原) 处理
	 */
	async onShrink(firstEle, lastEle) {
		// const restoreTarget = this.startElement
		// const endElement = this.endElement
		// const startEleClientRect = restoreTarget.getBoundingClientRect()
		// const endEleClientRect = endElement.getBoundingClientRect()
		// // console.log(endEleClientRect)
		//
		// let invertPosition = { x: endEleClientRect.left - startEleClientRect.left, y: endEleClientRect.top - startEleClientRect.top }
		// let invertPX = { width: endEleClientRect.width / startEleClientRect.width, height: endEleClientRect.height / startEleClientRect.height }

		let first = firstEle.getBoundingClientRect()
		let last = lastEle.getBoundingClientRect()

		let invertPosition = { x: last.left - first.left, y: last.top - first.top }
		let invertPX = { width: last.width / first.width, height: last.height / first.height }

		Velocity(firstEle, {
			translateX: [0, invertPosition.x],
			translateY: [0, invertPosition.y],
			scaleX: [1, invertPX.width],
			scaleY: [1, invertPX.height]
		}, {
			duration: 460,
			easing: [.4, 0, .22, 1],
			complete: () => {
				// this.resetZoomedEleAttr()
				lastEle.style.display = ''
				firstEle.style.transform = ''
				firstEle.style.visibility = ''
				// console.dir(endElement)
				this.setTransOrigin(firstEle,true)
			}
		})
	}

}
