// Any plugins you want to use has to be imported
// Detail plugins list see https://www.tinymce.com/docs/plugins/
// Custom builds see https://www.tinymce.com/download/custom-builds/
//code codesample insertdatetime media emoticons anchor preview fullscreen quickbars colorpicker textcolor contextmenu

//image imagetools
const plugins = [
	' advlist autolink autosave directionality hr ' +
	' link lists' +
	' colorpicker textcolor contextmenu' +
	' nonbreaking noneditable pagebreak paste print save searchreplace spellchecker tabfocus table template textpattern visualblocks visualchars'
]

export default plugins
