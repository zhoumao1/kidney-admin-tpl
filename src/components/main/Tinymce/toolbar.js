// Here is a list of the toolbar
// Detail list see https://www.tinymce.com/docs/advanced/editor-control-identifiers/#toolbarcontrols

// subscript superscript preview
//searchreplace fontselect
const toolbar = [
	'bold italic underline strikethrough forecolor backcolor fontsizeselect ',
	'alignleft aligncenter alignright outdent indent | blockquote undo redo removeformat code codesample',
	'hr bullist numlist link charmap  anchor pagebreak insertdatetime media table emoticons  fullscreen']
//image
export default toolbar
