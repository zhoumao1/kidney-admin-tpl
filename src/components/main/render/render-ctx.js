/**
 * @typedef rowProp
 * @property   {string}    key             标识符, 用于获取指定 key 的值
 * @property   {string}    value        默认展示的数据
 * @property   {function(h: function, params: Object): h}  [render]    Vue jsx 语法
 * @property   {string}    [slot]      插槽名
 * @property   {string}    [display]   css display
 */
export default {
	name: 'render-ctx',
	functional: true,
	props: {
		item: Object,
		defaultValue: [Number, String, Boolean, Array, Object, null, undefined],
		display: {
			type: String,
			default: 'block'
		}
	},
	render: (h, ctx) => {
		let renderType, props = ctx.props,
			/**@type {rowProp}*/
			item = props.item
		let flagKey = item.key
		let defaultValue = props.defaultValue || item.value

		if(!flagKey){
			throw ` props.item 中 缺少必要属性: key`
		}

		if(defaultValue){
			item.value = defaultValue
		}


		if(item.render){
			renderType = 'render'
		}else if (item.slot){
			renderType = 'slot'
		}
		if(renderType === 'render'){
			const params = {
				item: item,
				key: flagKey,
				value: defaultValue
			};
			return item.render(h, params);
		}

		if(renderType === 'slot'){
			if(!ctx.scopedSlots[item.slot]){
				throw `请检查是否有 名为 ${item.slot} 的插槽`
			}
			return h('div', {
				style: {
					display: item.display || props.display
				}
			}, ctx.scopedSlots[item.slot]({
				item: item,
				key: flagKey,
				value: defaultValue
			}))
		}
	}
};

