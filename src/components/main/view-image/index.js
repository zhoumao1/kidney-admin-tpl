import Vue from 'vue'
import __components_loading from './view-image'
import { afterLeave, deepMerge, lockClick } from './utils';

const isServer = Vue.prototype.$isServer
const ViewImageConstructor = Vue.extend(__components_loading)

let defaultOptions = {
	exist: true,


	// onClose: null,
	// onOpened: null,
	// duration: 2000,
};
let queue = []
let multiple = true

function createLoadInstance() {
	/* istanbul ignore if */

	if (!queue.length || multiple) {
		let newViewImage = new (Vue.extend(ViewImageConstructor))({
			el: document.createElement('div')
		});
		newViewImage.$on('input', function (value) {
			newViewImage.value = value;
		});
		queue.push(newViewImage);
		// console.log(queue, '创建')
	}

	return queue[queue.length - 1];
}

const ViewImage = (options) => {
	if (options === void 0) {
		options = {};
	}
	let parent = options.el || document.body
	let loadInstance = createLoadInstance()

	if (loadInstance.exist) {
		// console.log('已存在')
		// l.updateZIndex();
	}
	options = { ...defaultOptions, ...options }

	options.close = function () {
		loadInstance.exist = false;

		if (options.onClose) {
			options.onClose();
		}
		if (multiple && !isServer) {
			// eslint-disable-next-line no-unused-vars
			afterLeave(loadInstance, _ => {
				// const target = this.el || document.body
				removeTemplate(loadInstance)
				queue = queue.filter(function (item) {
					return item !== loadInstance;
				});
			}, 300);
			this.showPopup = false;
			lockClick(false, parent)
		}
	};
	// options.success = function (content) {
	// 	this.content = content || '处理成功'
	// 	this.showLoadIcon = false
	// 	this.color = '#2ea44f'
	// 	const timer = setTimeout(() =>{
	// 		this.close()
	// 		clearTimeout(timer)
	// 	}, 1000)
	// 	//
	// };

	Object.assign(loadInstance, options)
	parent.appendChild(loadInstance.$el)


	loadInstance.showPopup = true
	if(options.el){
		loadInstance.$el.style.position = 'absolute'
	}else {
		loadInstance.$el.style.position = ''
	}
	for (const defaultOptionsKey in options) {
		loadInstance[defaultOptionsKey] = options[defaultOptionsKey]
		// console.log(loadInstance[defaultOptionsKey], defaultOptionsKey)
	}
	// console.log(loadInstance, 'loadInstance')
	// clearTimeout(l.timer);
	//
	// if (options.duration > 0) {
	// 	l.timer = setTimeout(function () {
	// 		l.clear();
	// 	}, options.duration);
	// }

	return loadInstance;
}
/**
 * 关闭加载
 * @param all  {boolean=}  是否关闭全部
 */
ViewImage.close = function (all) {
	if (queue.length) {
		if (all) {
			queue.forEach(function (load) {
				load.close();
			});
			queue = [];
		} else if (!multiple) {
			queue[0].close();
		} else {
			queue.shift()
			.close();
			// console.log(queue.shift())
		}
	}
};
ViewImage.show = (options) => {
	if (!options) {
		options = deepMerge(defaultOptions)
		return ViewImage({ ...options })
	}
	if (typeof options === 'string') {
		options = { content: options }
		return ViewImage({ ...options })
	}else if (!options.el){
		return ViewImage({ ...options })
	}else if (typeof options.el === 'object'){
		if(!options.el.tagName)throw 'options.el type is Element or Document'
		options.el.style.position = 'relative'
		return ViewImage({ ...options })
	}else {
		return new Promise((resolve) => {
			setTimeout(() => {
				if (options && options.el) {
					document.querySelector(options.el).style.position = 'relative'
					options.el && options.el.includes('.') ? options.el_class = options.el : options.el_id = options.el
					options.el = document.querySelector(options.el)
					// .appendChild(document.createElement('div'))
				}
				options = { ...options }
				// return ViewImage({ ...options })
				return resolve(ViewImage({ ...options }))
			})
		})
	}
}

function removeTemplate(template) {
	if (template.$el && template.$el.parentNode) {
		template.$el.parentElement.style.position = ''
		template.$el.parentNode.removeChild(template.$el);
	}
	template.$destroy();
	// delete loadHash[key]
}

window.addEventListener('popstate', () => {
	ViewImage.close(true)
})
export default {
	install(Vue) {
		Vue.prototype.$ViewImage = ViewImage;
	},
	ViewImage
};

