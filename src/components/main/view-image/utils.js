/**
 * 绑定 after-leave 事件在 Vue 的子实例中, 确保动画结束后可以执行
 *
 * @param {Vue} instance Vue instance.
 * @param {Function} callback callback of after-leave event
 * @param options    { { event: string, speed: number, once: boolean } }
 * @property options.event 过渡结束后触发的事件名
 * @property options.speed 过渡速度，默认值为220ms
 * @property options.once  是否只绑定一次事件. 默认值为false。
 */
export const afterLeave = (instance, callback, options = { event: '', speed: 220, once: false }) => {
	if(!instance || !callback) throw new Error('instance & callback is required');
	let called = false;
	const afterLeaveCallback = function () {
		if(called) return;
		called = true;
		if(callback) {
			callback.apply(null, arguments);
		}
	};
	if(options.event){
		if (options.once) {
			instance.$once(options.event, afterLeaveCallback);
		} else {
			instance.$on(options.event, afterLeaveCallback);
		}
	}else {
		if (options.once) {
			instance.$once('after-leave', afterLeaveCallback);
		} else {
			instance.$on('after-leave', afterLeaveCallback);
		}
		setTimeout(() => {
			afterLeaveCallback();
		}, options.speed + 50);
	}
};


/**
 * 深度合并对象, 并不是引用指针
 * @param source
 * @return {null}
 */
export const deepMerge = (...source) =>{
	const result = Object.create(null)
	const deepCopy = (target) =>{

		let copyed_objs = [];//此数组解决了循环引用和相同引用的问题，它存放已经递归到的目标对象
		function _deepCopy(target){
			if((typeof target !== 'object')||!target){return target;}if(!['[object Object]', '[object Array]'].includes(Object.prototype.toString.call(target)))return target
			for(let i= 0 ;i<copyed_objs.length;i++){
				if(copyed_objs[i].target === target){
					return copyed_objs[i].copyTarget;
				}
			}
			let obj = {};
			if(Array.isArray(target)){
				obj = [];//处理target是数组的情况
			}
			copyed_objs.push({target:target,copyTarget:obj})
			Object.keys(target).forEach(key=>{
				if(obj[key]){ return;}
				obj[key] = _deepCopy(target[key]);
			});
			return obj;
		}
		return _deepCopy(target);
	}
	source.forEach(item =>{
		if(item){
			Object.assign(result, deepCopy(item))
		}
	})


	return result
}

var lockCount = 0;

export const lockClick =(lock) =>{
	if (lock) {
		if (!lockCount) {
			document.body.classList.add('unclickable');
		}

		lockCount++;
	} else {
		lockCount--;

		if (!lockCount) {
			document.body.classList.remove('unclickable');
		}
	}
}
