import { HttpRequest } from '@/libs/axios'

const axios = new HttpRequest('')
/**
 * HttpGet请求
 * @param {string}   url
 * @param {undefined|Object}  params
 * @param {boolean}  [showloading]
 * @param {Object}   [request_info]
 * @param {Object}   [response_info]
 * @param {string}   [successtip]
 * @param {string}   [failuretip]
 * @return {*}
 */
export const httpget = ({
	url,
	params,
	showloading,
	request_info,
	response_info,
	successtip = '数据获取成功',
	failuretip = '数据获取失败'
}) => {
	////debugger
	return axios.request({
		url: url,
		params: params,
		method: 'get',
		request_info, response_info,
		showinfo: {
			showloading: showloading,
			successtip: successtip,
			failuretip: failuretip
		}
	})
}

/**
 * HttpGet请求
 * @param {string}   url
 * @param {Object}  data
 * @param {boolean}  [showloading]
 * @param {boolean}  [is_cancel]
 * @param {Object}   [request_info]
 * @param {Object}   [response_info]
 * @param {string}   [successtip]
 * @param {string}   [failuretip]
 * @return {*}
 */
export const httppost = ({
	url,
	data,
	showloading,
	is_cancel,
	request_info,
	response_info,
	successtip = '操作成功',
	failuretip = '操作失败'
}) => {
	return axios.request({
		url: url,
		data: data,
		method: 'post',
		request_info,
		is_cancel, response_info,
		showinfo: {
			showloading: showloading,
			successtip: successtip,
			failuretip: failuretip
		}
	})
}
