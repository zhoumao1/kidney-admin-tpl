import axios from 'axios'
import { makeSign } from '@/proving/api.js'
import {
	getToken,
	setToken
} from '@/utils/auth'
import { Message } from 'element-ui'
import Load from 'vue-blur-load/src/load';

let requestPendingList = new Map()
/**
 * 添加请求
 * @param {Object} config
 */
const addPending = (config) => {
	config.cancelToken = config.cancelToken || new axios.CancelToken(cancel => {
		if (!requestPendingList.has(config.url)) { // 如果 requestPendingList 中不存在当前请求，则添加进去
			let exclude = ['BatchGeneratePresignedUri', 'SetOSSResourcePublicReadOnlyForWorker']
			if (exclude.find(item => config.url.includes(item))) return
			requestPendingList.set(config.url, cancel)
		}
	})
}
/**
 * 移除请求
 * @param {Object} config
 */
const removePending = (config) => {
	if (requestPendingList.has(config.url)) { // 如果在 requestPendingList 中存在当前请求标识，需要取消当前请求，并且移除
		const cancel = requestPendingList.get(config.url)
		cancel(config.url)
		requestPendingList.delete(config.url)
	}
}
/**
 * 清空 pending 中的请求（在路由跳转时调用）
 */
export const clearPending = () => {
	for (const [url, cancel] of requestPendingList) {
		cancel(url)
	}
	requestPendingList.clear()
}

export class HttpRequest {
	constructor(baseUrl = '') {
		// this.baseUrl = baseUrl
		this.queue = {}
	}

	getInsideConfig(request_info, response_info) {
		const config = {
			// baseURL: 'http://ko.ishenzang.com:1234',
			// baseURL: 'http://ko.ishenzang.com:1002', // 测试版
			// baseURL: 'https://ko.ishenzang.com', // 正式版
			// baseURL: 'http://api.shensx.com',
			headers: {
				...request_info
				//
			},
			timeout: 12000
		}
		return Object.assign(config, response_info)
	}

	destroy(url) {
		delete this.queue[url]
		if (!Object.keys(this.queue).length) {
		}
	}

	// 重新发起请求
	retry(instance, error) {

		Message.error('请求超时, 请检查网络')
		/*
				Message.error({
					duration: 10,
					showClose: true,
					render: h =>{
						return h('p', [
							h('span', '请求超时'),
							// h('span', {
							// 	style: {
							// 		'color': '#2f8df1',
							// 		'cursor': 'pointer'
							// 	},
							// 	on: {
							// 		click:() =>{
							// 			Message.destroy()
							// 			// Message.closeAll()
							// 			instance.request(error.config)
							// 		}
							// 	}
							// }, '点击重试')
						])
					}
				});
		*/
	}

	interceptors(instance, url) {
		// 请求拦截
		instance.interceptors.request.use(
			(config) => {
				if (!('onUploadProgress' in config)) {
					removePending(config)
					addPending(config)
				}
				let token = getToken()
				// console.log(token, 'token')

				//let token = '9b30c0801c6a248df431a9b07efcb71f'
				if (token) {
					// config.headers.Authorization = 'Bearer ' + token
				}
				let p = config.params
				for (let i in p) {
					if (p[i] === '' || p[i] === null) {
						delete p[i]
					}
				}
				let result = null
				if (token) {
					result = makeSign(p || config.data,
						config.method.toUpperCase(), token)
				} else {
					result = makeSign(p || config.data,
						config.method.toUpperCase())
				}
				let sign = result.sign

				// 在发送请求之前做些什么
				config.url += result.queryString
				// console.log('你好')
				//  let aliyun = request(config);

				// config.headers = {

				//阿里网关鉴权
				// "Content-MD5": aliyun.Md5, // MD5 校验。
				// // "X-Ca-Nonce": aliyun.Nonce, //结合时间戳防重放
				// "X-Ca-Key": aliyun.AppKey, //必选
				// "X-Ca-Signature": aliyun.Signature, //签名字符串 必选
				// //"X-Ca-SignatureMethod": 'HmacSHA256',
				// "X-Ca-Signature-Headers": aliyun.SignatureHeaders,
				// 'X-Ca-Stage': 'TEST',
				// 'X-Ca-Request-Mode': 'debug',
				// 'Accept': 'application/json',
				// 'X-Ca-Timestamp': aliyun.Date,
				// 'Content-Type': ''

				//  }
				if (config.showinfo && config.showinfo.showloading) {
				}
				config.headers['pretty_json'] = 'on'
				config.headers['SIGN'] = sign
				config.headers['AppID'] = 'customer.ssx.patient'
				if (token !== false) {
					config.headers['TOKEN'] = token
				}
				// console.log(config.headers, 'headers')
				return config
			},
			function (error) {
				console.log(error, 'aaaerror')
				// 对请求错误做些什么
				return Promise.reject(error)
			}
		)
		// 响应拦截
		instance.interceptors.response.use(
			(res) => {
				removePending(res.config)

				if (res.config.showinfo && res.config.showinfo.showloading) {
				}
				if (res.config.showinfo && res.config.showinfo.showloading && res.config.showinfo.successtip) {
					Message.success(res.config.showinfo.successtip)
				}
				// 递归的将字典的key转为小写
				let trans = function (data) {
					if (!data) return data
					if (data.constructor === Object) {
						for (let i in data) {
							if (data[i] !== null) {
								data[i.toLowerCase()] = trans(data[i])
							}
						}
					} else if (data.constructor === Array) {
						for (let i in data) {
							data[i] = trans(data[i])
						}
					}
					return data
				}
				res.data = trans(res.data)
				return res
			}, error => {
				Load.close(true)
				console.dir(error)
				console.log(error.message, error.code)
				// 报错信息没法获取config
				if (axios.isCancel(error)) {
					console.log('repeated request: ' + error.message)
				} else {
					if (error.message.includes('timeout')) {
						this.retry(instance, error)
						return Promise.reject(error)
					}
					if(error.code === "ECONNABORTED"){
						this.retry(instance, error)
						return Promise.reject(error)
					}
					if (error.response.status !== 410) {
						if (error.response.status === 401) {
							Message.error('请重新登录')
							setToken('')
						}
						if (error.config.showinfo && error.config.showinfo.failuretip) {
							Message.error(error.config.showinfo.failuretip)
						}

					} else {
						console.log(error.response.data.ERRORINFO, 'error.response.data.errorinfo')
						Message.error(error.response.data.ERRORINFO)
					}
					// if (error.response.status === 410) {
					// 	console.log(1111, 410, error)
					// 	if (error.response.data.ERRORINFO.length > 0) {
					// 		if (error.response.data.LEVEL === 1) {
					// 			console.log(error.response.data.ERRORINFO)
					// 			Message.error(error.response.data.ERRORINFO)
					// 		} else {
					// 			Message.error(error.response.data.ERRORINFO)
					// 			console.log(error.response.data.ERRORINFO)
					// 		}
					// 	}
					// }

					return Promise.reject(error)
				}
			}
		)
	}

	request(options) {
		const instance = axios.create()
		options = Object.assign(this.getInsideConfig(options.request_info, options.response_info), options)
		this.interceptors(instance, options.url)
		return instance(options)
	}
}

