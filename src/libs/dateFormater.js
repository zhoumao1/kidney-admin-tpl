import moment from "moment";
import 'moment/locale/zh-cn';
moment.locale('zh-cn');
//获取减8 --转换为日期
export function getTime(unix, format) {
    if (!format) {
        format = "YYYY-MM-DD HH:mm:ss";
    }
    if (unix < 1e10) {
        unix = (unix - 8 * 3600) * 1000;
    } else {
        unix = unix - 8 * 3600 * 1000;
    }
    return moment(unix).format(format);
}
// 获取时间戳
export function getTimeUnix(unix) {
    if (unix < 1e10) {
        unix = (unix - 8 * 3600) * 1000;
    } else {
        unix = unix - 8 * 3600 * 1000;
    }
    return unix;
}

/**
 * 获取 unix 的23:59:59
 * @param unix    {number}    时间戳
 * @return {number}  返回毫秒
 */
export function getEndTimeUnix(unix){
	let endTime
	if (unix > 1e10) {
		endTime = Math.floor(moment(unix).endOf('day').valueOf())
	}else {
		endTime = Math.floor(moment(unix * 1000).endOf('day').valueOf())
	}
	return endTime
}
//上传加8  --添加8小时时间戳
export function uploadTime(string) {
    return moment(string).unix() + 8 * 3600;
}

//时间戳转换 日期
export function stringFormater(string, format) {
    if (!format) {
        format = "YYYY-MM-DD";
    }
    return moment(string).format(format);
}
