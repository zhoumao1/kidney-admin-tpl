export const ParamInfoType = ['阴性', '+', '++', '+++', '++++', '±'];
export const ParamInfo = [
	{
		ID: 'FE',
		IsExportExcel: 'true',
		Display: '血清铁',
		Unit: 'umol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '9',
			Max: '29'
		},
		WaringRangeValue: {
			Min: '9',
			Max: '29'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_ANEMIA',
		TableDisplay: '贫血五项',
		FieldType: '0',
		FieldName: 'FE'
	},
	{
		ID: 'FERRITIN',
		IsExportExcel: 'true',
		Display: '铁蛋白',
		Unit: 'ng/ml',
		EditShow: 'true',
		RangeValue: {
			Min: '23.9',
			Max: '336.2'
		},
		WaringRangeValue: {
			Min: '23.9',
			Max: '336.2'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_ANEMIA',
		TableDisplay: '贫血五项',
		FieldType: '0',
		FieldName: 'FERRITIN'
	},
	{
		ID: 'TIBC',
		IsExportExcel: 'true',
		Display: '总铁结合力',
		Unit: 'umol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '55',
			Max: '77'
		},
		WaringRangeValue: {
			Min: '55',
			Max: '77'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_ANEMIA',
		TableDisplay: '贫血五项',
		FieldType: '0',
		FieldName: 'TIBC'
	},
	{
		ID: 'FOLAT',
		IsExportExcel: 'true',
		Display: '叶酸',
		Unit: 'nmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '6.8',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: '6.8',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_ANEMIA',
		TableDisplay: '贫血五项',
		FieldType: '0',
		FieldName: 'FOLAT'
	},
	{
		ID: 'VB12',
		IsExportExcel: 'true',
		Display: '维生素B12',
		Unit: 'pmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '133',
			Max: '675'
		},
		WaringRangeValue: {
			Min: '133',
			Max: '675'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_ANEMIA',
		TableDisplay: '贫血五项',
		FieldType: '0',
		FieldName: 'VB12'
	},
	{
		ID: 'B_ALBUMIN',
		IsExportExcel: 'true',
		Display: '白蛋白',
		Unit: 'g/L',
		EditShow: 'true',
		RangeValue: {
			Min: '40',
			Max: '55'
		},
		WaringRangeValue: {
			Min: '20',
			Max: 'NA'
		},
		IsEarlyWarining: 'true',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'B_ALBUMIN'
	},
	{
		ID: 'SCREA',
		IsExportExcel: 'true',
		Display: '血肌酐',
		Unit: 'umol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '44',
			Max: '133'
		},
		WaringRangeValue: {
			Min: '44',
			Max: '133'
		},
		IsEarlyWarining: 'true',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'SCREA'
	},
	{
		ID: 'BUN',
		IsExportExcel: 'true',
		Display: '血尿素氮',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '1.8',
			Max: '7.1'
		},
		WaringRangeValue: {
			Min: '1.8',
			Max: '7.1'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'BUN'
	},
	{
		ID: 'UA',
		IsExportExcel: 'true',
		Display: '尿酸',
		Unit: 'umol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '150',
			Max: '420'
		},
		WaringRangeValue: {
			Min: '150',
			Max: '420'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'UA'
	},
	{
		ID: 'CYS_C',
		IsExportExcel: 'true',
		Display: '胱抑素C',
		RankValue: '3',
		Unit: 'mg/L',
		EditShow: 'true',
		RangeValue: {
			Min: '0.6',
			Max: '1.18'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'CYS_C'
	},
	{
		ID: 'K',
		IsExportExcel: 'true',
		Display: '血钾',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '3.5',
			Max: '5.3'
		},
		WaringRangeValue: {
			Min: '3.0',
			Max: '5.5'
		},
		IsEarlyWarining: 'true',
		IsDrawChart: 'false',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'K'
	},
	{
		ID: 'CA2',
		IsExportExcel: 'true',
		Display: '血钙',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '2.12',
			Max: '2.75'
		},
		WaringRangeValue: {
			Min: '1.8',
			Max: '3.0'
		},
		IsEarlyWarining: 'true',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'CA2'
	},
	{
		ID: 'P',
		IsExportExcel: 'true',
		Display: '血磷',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '0.96',
			Max: '1.62'
		},
		WaringRangeValue: {
			Min: '0.96',
			Max: '1.62'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'P'
	},
	{
		ID: 'HCO3',
		IsExportExcel: 'true',
		Display: '碳酸氢根',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '22',
			Max: '30'
		},
		WaringRangeValue: {
			Min: '15',
			Max: '35'
		},
		IsEarlyWarining: 'true',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'HCO3'
	},
	{
		ID: 'NA',
		IsExportExcel: 'true',
		Display: '血钠',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '137',
			Max: '147'
		},
		WaringRangeValue: {
			Min: '120',
			Max: '160'
		},
		IsEarlyWarining: 'true',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'NA'
	},
	{
		ID: 'BGLU',
		IsExportExcel: 'true',
		Display: '血糖',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '3.9',
			Max: '6.1'
		},
		WaringRangeValue: {
			Min: '3.0',
			Max: '22'
		},
		IsEarlyWarining: 'true',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'BGLU'
	},
	{
		ID: 'CL',
		IsExportExcel: 'true',
		Display: '血氯',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '99',
			Max: '110'
		},
		WaringRangeValue: {
			Min: '99',
			Max: '110'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'CL'
	},
	{
		ID: 'PA',
		IsExportExcel: 'true',
		Display: '前白蛋白',
		Unit: 'mg/L',
		EditShow: 'true',
		RangeValue: {
			Min: '170',
			Max: '420'
		},
		WaringRangeValue: {
			Min: '170',
			Max: '420'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'PA'
	},
	{
		ID: 'TG',
		IsExportExcel: 'true',
		Display: '甘油三脂',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'TG'
	},
	{
		ID: 'TC',
		IsExportExcel: 'true',
		Display: '总胆固醇',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'TC'
	},
	{
		ID: 'LDLC',
		IsExportExcel: 'true',
		Display: '低密度脂蛋白',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'LDLC'
	},
	{
		ID: 'HDLC',
		IsExportExcel: 'true',
		Display: '高密度脂蛋白',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'HDLC'
	},
	{
		ID: 'ALT',
		IsExportExcel: 'true',
		Display: '谷丙转氨酶',
		Unit: 'IU/L',
		EditShow: 'true',
		RangeValue: {
			Min: '9',
			Max: '50'
		},
		WaringRangeValue: {
			Min: '9',
			Max: '50'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'ALT'
	},
	{
		ID: 'AST',
		IsExportExcel: 'true',
		Display: '谷草转氨酶',
		Unit: 'IU/L',
		EditShow: 'true',
		RangeValue: {
			Min: '15',
			Max: '40'
		},
		WaringRangeValue: {
			Min: '15',
			Max: '40'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'AST'
	},
	{
		ID: 'TBIL',
		IsExportExcel: 'true',
		Display: '总胆红素',
		Unit: 'umol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '1.7',
			Max: '20'
		},
		WaringRangeValue: {
			Min: '1.7',
			Max: '20'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'TBIL'
	},
	{
		ID: 'DBIL',
		IsExportExcel: 'true',
		Display: '直接胆红素',
		Unit: 'umol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '6'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '6'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'DBIL'
	},
	{
		ID: 'ALP',
		IsExportExcel: 'true',
		Display: '碱性磷酸酶',
		Unit: 'IU/L',
		EditShow: 'true',
		RangeValue: {
			Min: '45',
			Max: '125'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '63'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'ALP'
	},
	{
		ID: 'GGT',
		IsExportExcel: 'true',
		Display: '转肽酶',
		Unit: 'IU/L',
		EditShow: 'true',
		RangeValue: {
			Min: '10',
			Max: '60'
		},
		WaringRangeValue: {
			Min: '10',
			Max: '60'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'GGT'
	},
	{
		ID: 'TP',
		IsExportExcel: 'true',
		Display: '总蛋白',
		Unit: 'g/L',
		EditShow: 'true',
		RangeValue: {
			Min: '65',
			Max: '85'
		},
		WaringRangeValue: {
			Min: '65',
			Max: '85'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'TP'
	},
	{
		ID: 'SOSM',
		IsExportExcel: 'true',
		Display: '血渗透压',
		Unit: 'mOsm/kg',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_BIOCHEMISTRY',
		TableDisplay: '血生化(肝功能等)',
		FieldType: '0',
		FieldName: 'SOSM'
	},
	{
		ID: 'WBC',
		IsExportExcel: 'true',
		Display: '白细胞',
		Unit: '×10^9/L',
		EditShow: 'true',
		RangeValue: {
			Min: '3.5',
			Max: '9.5'
		},
		WaringRangeValue: {
			Min: '2',
			Max: '15'
		},
		IsEarlyWarining: 'true',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_RT',
		TableDisplay: '血常规',
		FieldType: '0',
		FieldName: 'WBC'
	},
	{
		ID: 'HGB',
		IsExportExcel: 'true',
		Display: '血红蛋白',
		Unit: 'g/L',
		EditShow: 'true',
		RangeValue: {
			Min: '130',
			Max: '175'
		},
		WaringRangeValue: {
			Min: '70',
			Max: 'NA'
		},
		IsEarlyWarining: 'true',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_RT',
		TableDisplay: '血常规',
		FieldType: '0',
		FieldName: 'HGB'
	},
	{
		ID: 'PLT',
		IsExportExcel: 'true',
		Display: '血小板',
		Unit: '×10^9/L',
		EditShow: 'true',
		RangeValue: {
			Min: '125',
			Max: '350'
		},
		WaringRangeValue: {
			Min: '100',
			Max: 'NA'
		},
		IsEarlyWarining: 'true',
		IsDrawChart: 'true',
		OwnerTable: 'T_BLOOD_RT',
		TableDisplay: '血常规',
		FieldType: '0',
		FieldName: 'PLT'
	},
	{
		ID: 'KIDNEYB',
		IsExportExcel: 'false',
		Display: 'KIDNEYB',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_OTHER_OBJ',
		TableDisplay: '其他',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'PTH',
		IsExportExcel: 'true',
		Display: '甲状旁腺素',
		Unit: 'pg/ml',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_PTH',
		TableDisplay: '甲状旁腺素',
		FieldType: '0',
		FieldName: 'PTH'
	},
	{
		ID: 'USERNAME',
		IsExportExcel: 'false',
		Display: 'USERNAME',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'EMAIL',
		IsExportExcel: 'false',
		Display: 'EMAIL',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'GENDER',
		IsExportExcel: 'false',
		Display: 'GENDER',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'BIRTHDAY',
		IsExportExcel: 'false',
		Display: 'BIRTHDAY',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'PIC',
		IsExportExcel: 'false',
		Display: 'PIC',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'TEL',
		IsExportExcel: 'false',
		Display: 'TEL',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'SCORE',
		IsExportExcel: 'false',
		Display: 'SCORE',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'AREA',
		IsExportExcel: 'false',
		Display: 'AREA',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'NICKNAME',
		IsExportExcel: 'false',
		Display: 'NICKNAME',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'RELEASE_NUM',
		IsExportExcel: 'false',
		Display: 'RELEASE_NUM',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'COLLECT_NUM',
		IsExportExcel: 'false',
		Display: 'COLLECT_NUM',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'CARE_NUM',
		IsExportExcel: 'false',
		Display: 'CARE_NUM',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'FANS_NUM',
		IsExportExcel: 'false',
		Display: 'FANS_NUM',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'CREATE_TIME',
		IsExportExcel: 'false',
		Display: 'CREATE_TIME',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'TAG',
		IsExportExcel: 'false',
		Display: 'TAG',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'KIDNEYB'
	},
	{
		ID: 'PRCID',
		IsExportExcel: 'false',
		Display: 'PRCID',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '数据类型',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '2',
		FieldName: 'PRCID'
	},
	{
		ID: 'HEIGHT',
		IsExportExcel: 'true',
		Display: '身高',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '数据类型',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'HEIGHT'
	},
	{
		ID: 'OCCUPATION',
		IsExportExcel: 'false',
		Display: 'OCCUPATION',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '数据类型',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '2',
		FieldName: 'OCCUPATION'
	},
	{
		ID: 'MPHONE',
		IsExportExcel: 'false',
		Display: 'MPHONE',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '2',
		FieldName: 'MPHONE'
	},
	{
		ID: 'WECHAT',
		IsExportExcel: 'false',
		Display: 'WECHAT',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '2',
		FieldName: 'WECHAT'
	},
	{
		ID: 'QQ',
		IsExportExcel: 'false',
		Display: 'QQ',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '2',
		FieldName: 'QQ'
	},
	{
		ID: 'ADDRESS',
		IsExportExcel: 'false',
		Display: 'ADDRESS',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '2',
		FieldName: 'ADDRESS'
	},
	{
		ID: 'ZIPCODE',
		IsExportExcel: 'false',
		Display: 'ZIPCODE',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '2',
		FieldName: 'ZIPCODE'
	},
	{
		ID: 'DESIGNATEDHOSPITAL',
		IsExportExcel: 'false',
		Display: 'DESIGNATEDHOSPITAL',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '2',
		FieldName: 'DESIGNATEDHOSPITAL'
	},
	{
		ID: 'SSN',
		IsExportExcel: 'false',
		Display: 'SSN',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '2',
		FieldName: 'SSN'
	},
	{
		ID: 'DATE_RENALDISEASEONSET',
		IsExportExcel: 'true',
		Display: '肾病初发日期',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '肾病初发日期',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '3',
		FieldName: 'DATE_RENALDISEASEONSET'
	},
	{
		ID: 'DATE_RENALBIOPSY',
		IsExportExcel: 'true',
		Display: '肾穿刺日期',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '肾穿刺日期',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '3',
		FieldName: 'DATE_RENALBIOPSY'
	},
	{
		ID: 'CLINICALDIAGNOSIS',
		IsExportExcel: 'true',
		Display: '临床诊断',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '临床诊断',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '1',
		FieldName: 'CLINICALDIAGNOSIS'
	},
	{
		ID: 'RENALPATHDIAG',
		IsExportExcel: 'true',
		Display: '病理诊断',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '病理诊断',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '1',
		FieldName: 'RENALPATHDIAG'
	},
	{
		ID: 'CREA',
		IsExportExcel: 'true',
		Display: '起病时血肌酐',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '起病时血肌酐',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '1',
		FieldName: 'CREA'
	},
	{
		ID: 'PIC_RENALBIOPSY',
		IsExportExcel: 'false',
		Display: 'PIC_RENALBIOPSY',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '肾脏病理图片',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '4',
		FieldName: 'PIC_RENALBIOPSY'
	},
	{
		ID: 'HYPERTENSION',
		IsExportExcel: 'true',
		Display: '高血压',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '高血压',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '0',
		FieldName: 'HYPERTENSION'
	},
	{
		ID: 'BP',
		IsExportExcel: 'false',
		Display: 'BP',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '1',
		FieldName: 'BP'
	},
	{
		ID: 'DIABETESMELLITUS',
		IsExportExcel: 'true',
		Display: '糖尿病',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '糖尿病',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '1',
		FieldName: 'DIABETESMELLITUS'
	},
	{
		ID: 'SLE',
		IsExportExcel: 'true',
		Display: '红斑狼疮',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '红斑狼疮',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '1',
		FieldName: 'SLE'
	},
	{
		ID: 'HSP',
		IsExportExcel: 'true',
		Display: '过敏性紫癜',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '过敏性紫癜',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '1',
		FieldName: 'HSP'
	},
	{
		ID: 'SS',
		IsExportExcel: 'true',
		Display: '干燥综合症',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '干燥综合症',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '1',
		FieldName: 'SS'
	},
	{
		ID: 'SSC',
		IsExportExcel: 'true',
		Display: '系统性硬化',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '系统性硬化',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '1',
		FieldName: 'SSC'
	},
	{
		ID: 'MM',
		IsExportExcel: 'true',
		Display: '多发性骨髓瘤',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '1',
		FieldName: 'MM'
	},
	{
		ID: 'HBV',
		IsExportExcel: 'true',
		Display: '乙型肝炎',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '1',
		FieldName: 'HBV'
	},
	{
		ID: 'HCV',
		IsExportExcel: 'true',
		Display: '丙型肝炎',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '1',
		FieldName: 'HCV'
	},
	{
		ID: 'TUMOR_CANCER',
		IsExportExcel: 'true',
		Display: '肿瘤病史 ',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '1',
		FieldName: 'TUMOR_CANCER'
	},
	{
		ID: 'HYPERURICEMIA',
		IsExportExcel: 'true',
		Display: '高尿酸血症',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '1',
		FieldName: 'HYPERURICEMIA'
	},
	{
		ID: 'OTHERHISTORY',
		IsExportExcel: 'true',
		Display: '其他',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PATIENTINFO',
		TableDisplay: '病人信息',
		FieldType: '1',
		FieldName: 'OTHERHISTORY'
	},
	{
		ID: 'EGFR',
		IsExportExcel: 'true',
		Display: '估计肾小球滤过率',
		Unit: 'ml/min/1.73m^2',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_RENAL_FUNC',
		TableDisplay: '血肌酐清除率',
		FieldType: '0',
		FieldName: 'EGFR'
	},
	{
		ID: 'CCR',
		IsExportExcel: 'true',
		Display: '肌酐清除率',
		Unit: 'ml/min',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_RENAL_FUNC',
		TableDisplay: '血肌酐清除率',
		FieldType: '0',
		FieldName: 'CCR'
	},
	{
		ID: 'UTP',
		IsExportExcel: 'true',
		Display: '24h尿蛋白定量 ',
		Unit: 'g/24hr',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_SYMPTOMS',
		TableDisplay: '病情描述',
		FieldType: '0',
		FieldName: 'UTP'
	},
	{
		ID: 'SYMPTOMS',
		IsExportExcel: 'true',
		Display: '症状',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_SYMPTOMS',
		TableDisplay: '病情描述',
		FieldType: '2',
		FieldName: 'SYMPTOMS'
	},
	{
		ID: 'MA',
		IsExportExcel: 'true',
		Display: '尿微量白蛋白',
		Unit: 'mg/L',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '19'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '19'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_OTHERRESULT',
		TableDisplay: '肾早损',
		FieldType: '0',
		FieldName: 'MA'
	},
	{
		ID: 'UCREA',
		IsExportExcel: 'true',
		Display: '尿肌酐',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_OTHERRESULT',
		TableDisplay: '肾早损',
		FieldType: '0',
		FieldName: 'UCREA'
	},
	{
		ID: 'NAG',
		IsExportExcel: 'true',
		Display: '尿NAG酶',
		Unit: 'U/L',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '21'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '21'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_OTHERRESULT',
		TableDisplay: '肾早损',
		FieldType: '0',
		FieldName: 'NAG'
	},
	{
		ID: 'A1M',
		IsExportExcel: 'true',
		Display: 'α1-微球蛋白',
		Unit: 'mg/L',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '12.00'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '12.00'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_OTHERRESULT',
		TableDisplay: '肾早损',
		FieldType: '0',
		FieldName: 'A1M'
	},
	{
		ID: 'UOSM',
		IsExportExcel: 'true',
		Display: '尿渗透压',
		Unit: 'mOsm/kg',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_OTHERRESULT',
		TableDisplay: '肾早损',
		FieldType: '0',
		FieldName: 'UOSM'
	},
	{
		ID: 'UHCO3',
		IsExportExcel: 'true',
		Display: '尿碳酸氢盐',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_OTHERRESULT',
		TableDisplay: '肾早损',
		FieldType: '0',
		FieldName: 'UHCO3'
	},
	{
		ID: 'URINETITRATABLEACID',
		IsExportExcel: 'true',
		Display: '尿可滴定酸',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_OTHERRESULT',
		TableDisplay: '肾早损',
		FieldType: '0',
		FieldName: 'URINETITRATABLEACID'
	},
	{
		ID: 'UNH4PLUS',
		IsExportExcel: 'true',
		Display: '尿铵离子',
		Unit: 'mmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_OTHERRESULT',
		TableDisplay: '肾早损',
		FieldType: '0',
		FieldName: 'UNH4PLUS'
	},
	{
		ID: 'URBC_UL',
		IsExportExcel: 'true',
		Display: '尿红细胞 ',
		Unit: '/ul',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '25'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_URINE_ROUTINE',
		TableDisplay: '尿常规',
		FieldType: '0',
		FieldName: 'URBC_UL'
	},
	{
		ID: 'PRO',
		IsExportExcel: 'true',
		Display: '尿蛋白',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '5'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_ROUTINE',
		TableDisplay: '尿常规',
		FieldType: '5',
		FieldName: 'PRO'
	},
	{
		ID: 'BLD',
		IsExportExcel: 'true',
		Display: '尿潜血',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '5'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '5'
		},
		Remark: '几个+号 最多4个加号',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_ROUTINE',
		TableDisplay: '尿常规',
		FieldType: '5',
		FieldName: 'BLD'
	},
	{
		ID: 'NIT',
		IsExportExcel: 'true',
		Display: '亚硝酸盐',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '阴性阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_ROUTINE',
		TableDisplay: '尿常规',
		FieldType: '1',
		FieldName: 'NIT'
	},
	{
		ID: 'GLU',
		IsExportExcel: 'true',
		Display: '尿糖',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '4'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '4'
		},
		Remark: '几个+号 最多4个加号',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_ROUTINE',
		TableDisplay: '尿常规',
		FieldType: '1',
		FieldName: 'GLU'
	},
	{
		ID: 'KET',
		IsExportExcel: 'true',
		Display: '尿酮体',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '4'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '4'
		},
		Remark: '几个+号 最多4个加号',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_ROUTINE',
		TableDisplay: '尿常规',
		FieldType: '1',
		FieldName: 'KET'
	},
	{
		ID: 'PH',
		IsExportExcel: 'true',
		Display: '尿pH',
		EditShow: 'true',
		RangeValue: {
			Min: '4.5',
			Max: '8.0'
		},
		WaringRangeValue: {
			Min: '4.5',
			Max: '8.0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_URINE_ROUTINE',
		TableDisplay: '尿常规',
		FieldType: '0',
		FieldName: 'PH'
	},
	{
		ID: 'BIL',
		IsExportExcel: 'true',
		Display: '尿胆红素',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '4'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '4'
		},
		Remark: '几个+号 最多4个加号',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_ROUTINE',
		TableDisplay: '尿常规',
		FieldType: '1',
		FieldName: 'BIL'
	},
	{
		ID: 'URO',
		IsExportExcel: 'true',
		Display: '尿胆原',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '4'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '4'
		},
		Remark: '几个+号 最多4个加号',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_ROUTINE',
		TableDisplay: '尿常规',
		FieldType: '1',
		FieldName: 'URO'
	},
	{
		ID: 'SG',
		IsExportExcel: 'true',
		Display: '尿比重',
		EditShow: 'true',
		RangeValue: {
			Min: '1.003',
			Max: '1.035'
		},
		WaringRangeValue: {
			Min: '1.003',
			Max: '1.035'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_URINE_ROUTINE',
		TableDisplay: '尿常规',
		FieldType: '0',
		FieldName: 'SG'
	},
	{
		ID: 'UWBC',
		IsExportExcel: 'true',
		Display: '尿白细胞',
		Unit: '/HP',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '5'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '5'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_URINE_ROUTINE',
		TableDisplay: '尿常规',
		FieldType: '0',
		FieldName: 'UWBC'
	},
	{
		ID: 'URBC',
		IsExportExcel: 'true',
		Display: '尿红细胞',
		Unit: '/HP',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '3'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '3'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_URINE_ROUTINE',
		TableDisplay: '尿常规',
		FieldType: '0',
		FieldName: 'URBC'
	},
	{
		ID: 'UMWBC',
		IsExportExcel: 'true',
		Display: '尿白细胞',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '4'
		},
		WaringRangeValue: {
			Min: '0',
			Max: 'NA'
		},
		Remark: '阴性阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_ROUTINE',
		TableDisplay: '尿常规',
		FieldType: '1',
		FieldName: 'UMWBC'
	},
	{
		ID: 'CP',
		IsExportExcel: 'true',
		Display: '收缩压',
		Unit: 'mmHg',
		EditShow: 'true',
		RangeValue: {
			Min: '90',
			Max: '140'
		},
		WaringRangeValue: {
			Min: '90',
			Max: '140'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_BPRECORD',
		TableDisplay: '血压',
		FieldType: '0',
		FieldName: 'CP'
	},
	{
		ID: 'DP',
		IsExportExcel: 'true',
		Display: '舒张压',
		Unit: 'mmHg',
		EditShow: 'true',
		RangeValue: {
			Min: '60',
			Max: '90'
		},
		WaringRangeValue: {
			Min: '60',
			Max: '90'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_BPRECORD',
		TableDisplay: '血压',
		FieldType: '0',
		FieldName: 'DP'
	},
	{
		ID: 'REMARK',
		IsExportExcel: 'false',
		Display: 'REMARK',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_BPRECORD',
		TableDisplay: '血压',
		FieldType: '2',
		FieldName: 'REMARK'
	},
	{
		ID: 'DRUGID',
		IsExportExcel: 'true',
		Display: 'DRUGID',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_TREATMENT',
		TableDisplay: '用药信息',
		FieldType: '2',
		FieldName: 'DRUGID'
	},
	{
		ID: 'TID',
		IsExportExcel: 'false',
		Display: '用药序号',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_TREATMENT',
		TableDisplay: '用药信息',
		FieldType: '3',
		FieldName: 'TID'
	},
	{
		ID: 'STARTDATE',
		IsExportExcel: 'true',
		Display: '开始用药时间',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_TREATMENT',
		TableDisplay: '用药信息',
		FieldType: '3',
		FieldName: 'STARTDATE'
	},
	{
		ID: 'ENDDATE',
		IsExportExcel: 'true',
		Display: '终止用药时间',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_TREATMENT',
		TableDisplay: '用药信息',
		FieldType: '3',
		FieldName: 'ENDDATE'
	},
	{
		ID: 'DRUGSPEC',
		IsExportExcel: 'true',
		Display: '药物剂型',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_TREATMENT',
		TableDisplay: '用药信息',
		FieldType: '3',
		FieldName: 'DRUGSPEC'
	},
	{
		ID: 'FREQUENCE',
		IsExportExcel: 'true',
		Display: '用药频率',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_TREATMENT',
		TableDisplay: '用药信息',
		FieldType: '2',
		FieldName: 'FREQUENCE'
	},
	{
		ID: 'ANA',
		IsExportExcel: 'true',
		Display: '抗核抗体',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ANA',
		TableDisplay: '抗核抗体',
		FieldType: '1',
		FieldName: 'ANA'
	},
	{
		ID: 'XANCA',
		IsExportExcel: 'true',
		Display: '不典型ANCA',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '不典型抗中性粒细胞抗体',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ANCA',
		TableDisplay: 'ANCA',
		FieldType: '1',
		FieldName: 'XANCA'
	},
	{
		ID: 'CANCA',
		IsExportExcel: 'true',
		Display: 'C-ANCA',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '阴性阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ANCA',
		TableDisplay: 'ANCA',
		FieldType: '1',
		FieldName: 'CANCA'
	},
	{
		ID: 'PANCA',
		IsExportExcel: 'true',
		Display: 'P-ANCA',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '阴性阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ANCA',
		TableDisplay: 'ANCA',
		FieldType: '1',
		FieldName: 'PANCA'
	},
	{
		ID: 'PR3',
		IsExportExcel: 'true',
		Display: 'PR3',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '阴性阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ANCA',
		TableDisplay: 'ANCA',
		FieldType: '1',
		FieldName: 'PR3'
	},
	{
		ID: 'MPO',
		IsExportExcel: 'true',
		Display: 'MPO',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '阴性阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ANCA',
		TableDisplay: 'ANCA',
		FieldType: '1',
		FieldName: 'MPO'
	},
	{
		ID: 'COLOR',
		IsExportExcel: 'true',
		Display: '颜色',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_BCG',
		TableDisplay: '粪便常规',
		FieldType: '2',
		FieldName: 'COLOR'
	},
	{
		ID: 'MICROSCOPY',
		IsExportExcel: 'true',
		Display: '镜检',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_BCG',
		TableDisplay: '粪便常规',
		FieldType: '2',
		FieldName: 'MICROSCOPY'
	},
	{
		ID: 'OCCULTBLOOD',
		IsExportExcel: 'true',
		Display: '隐血',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '阴性阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_BCG',
		TableDisplay: '粪便常规',
		FieldType: '1',
		FieldName: 'OCCULTBLOOD'
	},
	{
		ID: 'CBLOOD',
		IsExportExcel: 'true',
		Display: '化学法隐血',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '阴性阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_BCG',
		TableDisplay: '粪便常规',
		FieldType: '1',
		FieldName: 'CBLOOD'
	},
	{
		ID: 'IBLOOD',
		IsExportExcel: 'true',
		Display: '免疫法隐血',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '阴性阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_BCG',
		TableDisplay: '粪便常规',
		FieldType: '1',
		FieldName: 'IBLOOD'
	},
	{
		ID: 'CG',
		IsExportExcel: 'true',
		Display: '冷球蛋白',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '阴性阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_CG',
		TableDisplay: '冷球蛋白',
		FieldType: '1',
		FieldName: 'CG'
	},
	{
		ID: 'C3',
		IsExportExcel: 'true',
		Display: '补体C3',
		Unit: 'g/L',
		EditShow: 'true',
		RangeValue: {
			Min: '0.8',
			Max: '1.28'
		},
		WaringRangeValue: {
			Min: '0.8',
			Max: '1.28'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_COM',
		TableDisplay: '补体',
		FieldType: '0',
		FieldName: 'C3'
	},
	{
		ID: 'C4',
		IsExportExcel: 'true',
		Display: '补体C4',
		Unit: 'g/L',
		EditShow: 'true',
		RangeValue: {
			Min: '0.1',
			Max: '0.4'
		},
		WaringRangeValue: {
			Min: '0.1',
			Max: '0.4'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_COM',
		TableDisplay: '补体',
		FieldType: '0',
		FieldName: 'C4'
	},
	{
		ID: 'GHBA1C',
		IsExportExcel: 'true',
		Display: '糖化血红蛋白',
		Unit: '%',
		EditShow: 'true',
		RangeValue: {
			Min: '4',
			Max: '6'
		},
		WaringRangeValue: {
			Min: '4',
			Max: '6'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_DM',
		TableDisplay: '糖化血红蛋白',
		FieldType: '0',
		FieldName: 'GHBA1C'
	},
	{
		ID: 'TACROLIMUS',
		IsExportExcel: 'true',
		Display: '他克莫司(FK506)',
		Unit: 'ng/ml',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: '10',
			Max: '20'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_DRUG',
		TableDisplay: '血药浓度',
		FieldType: '0',
		FieldName: 'TACROLIMUS'
	},
	{
		ID: 'CYCLOSPORINE',
		IsExportExcel: 'true',
		Display: '环孢素',
		Unit: 'ng/ml',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: '100',
			Max: '200'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_DRUG',
		TableDisplay: '血药浓度',
		FieldType: '0',
		FieldName: 'CYCLOSPORINE'
	},
	{
		ID: 'DSDNAIIF',
		IsExportExcel: 'true',
		Display: 'dsDNA定性',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '阴性阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_DSDNA',
		TableDisplay: 'dsDNA抗体',
		FieldType: '1',
		FieldName: 'DSDNAIIF'
	},
	{
		ID: 'DSDNAELISA',
		IsExportExcel: 'true',
		Display: 'dsDNA滴度',
		Unit: 'IU/ML',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: '0',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_DSDNA',
		TableDisplay: 'dsDNA抗体',
		FieldType: '0',
		FieldName: 'DSDNAELISA'
	},
	{
		ID: 'NRNP',
		IsExportExcel: 'true',
		Display: 'NRNP',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ENA',
		TableDisplay: 'ENA谱',
		FieldType: '1',
		FieldName: 'NRNP'
	},
	{
		ID: 'SM',
		IsExportExcel: 'true',
		Display: 'SM',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ENA',
		TableDisplay: 'ENA谱',
		FieldType: '1',
		FieldName: 'SM'
	},
	{
		ID: 'SSA',
		IsExportExcel: 'true',
		Display: 'SSA',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ENA',
		TableDisplay: 'ENA谱',
		FieldType: '1',
		FieldName: 'SSA'
	},
	{
		ID: 'SSB',
		IsExportExcel: 'true',
		Display: 'SSB',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ENA',
		TableDisplay: 'ENA谱',
		FieldType: '1',
		FieldName: 'SSB'
	},
	{
		ID: 'SCL70',
		IsExportExcel: 'true',
		Display: 'SCL70',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ENA',
		TableDisplay: 'ENA谱',
		FieldType: '1',
		FieldName: 'SCL70'
	},
	{
		ID: 'JO1',
		IsExportExcel: 'true',
		Display: 'JO1',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ENA',
		TableDisplay: 'ENA谱',
		FieldType: '1',
		FieldName: 'JO1'
	},
	{
		ID: 'CENPB',
		IsExportExcel: 'true',
		Display: '抗着丝点抗体',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ENA',
		TableDisplay: 'ENA谱',
		FieldType: '1',
		FieldName: 'CENPB'
	},
	{
		ID: 'RRNP',
		IsExportExcel: 'true',
		Display: '抗核糖体P蛋白抗体',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ENA',
		TableDisplay: 'ENA谱',
		FieldType: '1',
		FieldName: 'RRNP'
	},
	{
		ID: 'SMA',
		IsExportExcel: 'true',
		Display: 'SMA',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ENA',
		TableDisplay: 'ENA谱',
		FieldType: '1',
		FieldName: 'SMA'
	},
	{
		ID: 'AMA',
		IsExportExcel: 'true',
		Display: 'AMA',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '阴性（＜1:100）',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ENA',
		TableDisplay: 'ENA谱',
		FieldType: '1',
		FieldName: 'AMA'
	},
	{
		ID: 'ACL',
		IsExportExcel: 'true',
		Display: 'ACL',
		Unit: 'RU/ml',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '＜12RU/ml',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ENA',
		TableDisplay: 'ENA谱',
		FieldType: '0',
		FieldName: 'ACL'
	},
	{
		ID: 'ACA',
		IsExportExcel: 'true',
		Display: '抗着丝点抗体',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '阴性（＜1:100）',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ENA',
		TableDisplay: 'ENA谱',
		FieldType: '1',
		FieldName: 'ACA'
	},
	{
		ID: 'ESR',
		IsExportExcel: 'true',
		Display: '血沉',
		Unit: 'mm/h',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '15'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '15'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ESR',
		TableDisplay: '血沉',
		FieldType: '0',
		FieldName: 'ESR'
	},
	{
		ID: 'GBM',
		IsExportExcel: 'true',
		Display: 'GBM',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '抗肾小球基底膜抗体 RU/ml，选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_GBM',
		TableDisplay: 'GBM',
		FieldType: '1',
		FieldName: 'GBM'
	},
	{
		ID: 'HBSAG',
		IsExportExcel: 'true',
		Display: '乙肝表面抗原',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_GS',
		TableDisplay: '感染筛查',
		FieldType: '1',
		FieldName: 'HBSAG'
	},
	{
		ID: 'HBSAB',
		IsExportExcel: 'true',
		Display: '乙肝表面抗体',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_GS',
		TableDisplay: '感染筛查',
		FieldType: '1',
		FieldName: 'HBSAB'
	},
	{
		ID: 'HBCAB',
		IsExportExcel: 'true',
		Display: '乙肝核心抗体',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_GS',
		TableDisplay: '感染筛查',
		FieldType: '1',
		FieldName: 'HBCAB'
	},
	{
		ID: 'HBEAG',
		IsExportExcel: 'true',
		Display: '乙肝e抗原',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_GS',
		TableDisplay: '感染筛查',
		FieldType: '1',
		FieldName: 'HBEAG'
	},
	{
		ID: 'HBEAB',
		IsExportExcel: 'true',
		Display: '乙肝e抗体',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_GS',
		TableDisplay: '感染筛查',
		FieldType: '1',
		FieldName: 'HBEAB'
	},
	{
		ID: 'ANTIHIV',
		IsExportExcel: 'true',
		Display: '艾滋病病毒抗体',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_GS',
		TableDisplay: '感染筛查',
		FieldType: '1',
		FieldName: 'ANTIHIV'
	},
	{
		ID: 'ANTIHCV',
		IsExportExcel: 'true',
		Display: '丙肝病毒抗体',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_GS',
		TableDisplay: '感染筛查',
		FieldType: '1',
		FieldName: 'ANTIHCV'
	},
	{
		ID: 'TPAB',
		IsExportExcel: 'true',
		Display: '梅毒抗体',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_GS',
		TableDisplay: '感染筛查',
		FieldType: '1',
		FieldName: 'TPAB'
	},
	{
		ID: 'IGG',
		IsExportExcel: 'true',
		Display: '免疫球蛋白G',
		Unit: 'g/L',
		EditShow: 'true',
		RangeValue: {
			Min: '7.23',
			Max: '16.85'
		},
		WaringRangeValue: {
			Min: '7.23',
			Max: '16.85'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_IG',
		TableDisplay: '免疫球蛋白',
		FieldType: '0',
		FieldName: 'IGG'
	},
	{
		ID: 'IGA',
		IsExportExcel: 'true',
		Display: '免疫球蛋白A',
		Unit: 'g/L',
		EditShow: 'true',
		RangeValue: {
			Min: '0.69',
			Max: '3.82'
		},
		WaringRangeValue: {
			Min: '0.69',
			Max: '3.82'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_IG',
		TableDisplay: '免疫球蛋白',
		FieldType: '0',
		FieldName: 'IGA'
	},
	{
		ID: 'IGM',
		IsExportExcel: 'true',
		Display: '免疫球蛋白M',
		Unit: 'g/L',
		EditShow: 'true',
		RangeValue: {
			Min: '0.63',
			Max: '2.77'
		},
		WaringRangeValue: {
			Min: '0.63',
			Max: '2.77'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_IG',
		TableDisplay: '免疫球蛋白',
		FieldType: '0',
		FieldName: 'IGM'
	},
	{
		ID: 'CRCL',
		IsExportExcel: 'true',
		Display: '24h肌酐清除率',
		EditShow: 'true',
		RangeValue: {},
		WaringRangeValue: {},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_NCCR',
		TableDisplay: '尿肌酐清除率',
		FieldType: '0',
		FieldName: 'CRCL'
	},
	{
		ID: 'PT',
		IsExportExcel: 'true',
		Display: '凝血酶原时间',
		Unit: '秒',
		EditShow: 'true',
		RangeValue: {
			Min: '9.0',
			Max: '11.5'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '15'
		},
		IsEarlyWarining: 'true',
		IsDrawChart: 'false',
		OwnerTable: 'T_NXGN',
		TableDisplay: '凝血功能',
		FieldType: '0',
		FieldName: 'PT'
	},
	{
		ID: 'APTT',
		IsExportExcel: 'true',
		Display: '部分凝血活酶时间',
		Unit: '秒',
		EditShow: 'true',
		RangeValue: {
			Min: '26.9',
			Max: '37.6'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '45'
		},
		IsEarlyWarining: 'true',
		IsDrawChart: 'false',
		OwnerTable: 'T_NXGN',
		TableDisplay: '凝血功能',
		FieldType: '0',
		FieldName: 'APTT'
	},
	{
		ID: 'INR',
		IsExportExcel: 'true',
		Display: '国际标准化比值',
		EditShow: 'true',
		RangeValue: {
			Min: '0.89',
			Max: '1.13'
		},
		WaringRangeValue: {
			Min: '0.89',
			Max: '1.13'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_NXGN',
		TableDisplay: '凝血功能',
		FieldType: '0',
		FieldName: 'INR'
	},
	{
		ID: 'FIBC',
		IsExportExcel: 'true',
		Display: '纤维蛋白原',
		Unit: 'g/L',
		EditShow: 'true',
		RangeValue: {
			Min: '2',
			Max: '4'
		},
		WaringRangeValue: {
			Min: '1.5',
			Max: 'NA'
		},
		IsEarlyWarining: 'true',
		IsDrawChart: 'false',
		OwnerTable: 'T_NXGN',
		TableDisplay: '凝血功能',
		FieldType: '0',
		FieldName: 'FIBC'
	},
	{
		ID: 'DDIMMER',
		IsExportExcel: 'true',
		Display: 'D-二聚体',
		Unit: 'mg/L',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0.5'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_NXGN',
		TableDisplay: '凝血功能',
		FieldType: '0',
		FieldName: 'DDIMMER'
	},
	{
		ID: 'FDP',
		IsExportExcel: 'true',
		Display: 'FDP',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '5'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '5'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_NXGN',
		TableDisplay: '凝血功能',
		FieldType: '0',
		FieldName: 'FDP'
	},
	{
		ID: 'RBSPAPER',
		IsExportExcel: 'true',
		Display: 'B超报告结论语句',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '也即将B超上的结论录入数据库',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_RBS',
		TableDisplay: '肾血管超声',
		FieldType: '2',
		FieldName: 'RBSPAPER'
	},
	{
		ID: 'RBPAPER',
		IsExportExcel: 'true',
		Display: 'B超报告结论语句',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '也即将B超上的结论录入数据库',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_RB',
		TableDisplay: '超声检查',
		FieldType: '2',
		FieldName: 'RBPAPER'
	},
	{
		ID: 'ASO',
		IsExportExcel: 'true',
		Display: '抗链球菌溶血素O',
		Unit: 'IU/mL',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '200'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '200'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_RHE',
		TableDisplay: '类风湿三项',
		FieldType: '0',
		FieldName: 'ASO'
	},
	{
		ID: 'RF',
		IsExportExcel: 'true',
		Display: '类风湿因子',
		Unit: 'IU/mL',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '30'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '30'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_RHE',
		TableDisplay: '类风湿三项',
		FieldType: '0',
		FieldName: 'RF'
	},
	{
		ID: 'CRP',
		IsExportExcel: 'true',
		Display: 'C-反应蛋白',
		Unit: 'mg/L',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '8'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '8'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_RHE',
		TableDisplay: '类风湿三项',
		FieldType: '0',
		FieldName: 'CRP'
	},
	{
		ID: 'RMRPAPER',
		IsExportExcel: 'true',
		Display: '核磁报告结论语句',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_RMR',
		TableDisplay: '肾核磁',
		FieldType: '2',
		FieldName: 'RMRPAPER'
	},
	{
		ID: 'TT3',
		IsExportExcel: 'true',
		Display: '总T3',
		Unit: 'nmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '0.92',
			Max: '2.79'
		},
		WaringRangeValue: {
			Min: '0.92',
			Max: '2.79'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_TF',
		TableDisplay: '甲状腺功能',
		FieldType: '0',
		FieldName: 'TT3'
	},
	{
		ID: 'TT4',
		IsExportExcel: 'true',
		Display: '总T4',
		Unit: 'nmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '58.1',
			Max: '140.6'
		},
		WaringRangeValue: {
			Min: '58.1',
			Max: '140.6'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_TF',
		TableDisplay: '甲状腺功能',
		FieldType: '0',
		FieldName: 'TT4'
	},
	{
		ID: 'FT3',
		IsExportExcel: 'true',
		Display: '游离T3',
		Unit: 'pmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '3.50',
			Max: '6.50'
		},
		WaringRangeValue: {
			Min: '3.50',
			Max: '6.50'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_TF',
		TableDisplay: '甲状腺功能',
		FieldType: '0',
		FieldName: 'FT3'
	},
	{
		ID: 'F4',
		IsExportExcel: 'true',
		Display: '游离T4',
		Unit: 'pmol/L',
		EditShow: 'true',
		RangeValue: {
			Min: '11.48',
			Max: '22.70'
		},
		WaringRangeValue: {
			Min: '11.48',
			Max: '22.70'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_TF',
		TableDisplay: '甲状腺功能',
		FieldType: '0',
		FieldName: 'F4'
	},
	{
		ID: 'TSH',
		IsExportExcel: 'true',
		Display: 'TSH',
		Unit: 'uIU/mL',
		EditShow: 'true',
		RangeValue: {
			Min: '0.55',
			Max: '4.78'
		},
		WaringRangeValue: {
			Min: '0.55',
			Max: '4.78'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_TF',
		TableDisplay: '甲状腺功能',
		FieldType: '0',
		FieldName: 'TSH'
	},
	{
		ID: 'CEA',
		IsExportExcel: 'true',
		Display: '癌胚抗原 ',
		Unit: 'ng/mL',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '5'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '5'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_TM',
		TableDisplay: '肿瘤标志物',
		FieldType: '0',
		FieldName: 'CEA'
	},
	{
		ID: 'AFP',
		IsExportExcel: 'true',
		Display: '甲胎蛋白 ',
		Unit: 'ug/L',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '10.9'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '10.9'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_TM',
		TableDisplay: '肿瘤标志物',
		FieldType: '0',
		FieldName: 'AFP'
	},
	{
		ID: 'TPSA',
		IsExportExcel: 'true',
		Display: '总前列腺特异性抗原',
		Unit: 'ng/mL',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '4'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '4'
		},
		Remark: '选择阴性或阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_TM',
		TableDisplay: '肿瘤标志物',
		FieldType: '0',
		FieldName: 'TPSA'
	},
	{
		ID: 'FPSA',
		IsExportExcel: 'true',
		Display: '游离前列腺特异性抗原',
		Unit: 'ng/mL',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1.0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1.0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_TM',
		TableDisplay: '肿瘤标志物',
		FieldType: '0',
		FieldName: 'FPSA'
	},
	{
		ID: 'PN',
		IsExportExcel: 'true',
		Display: '尿免疫固定电泳',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '阴性阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_UIFE',
		TableDisplay: '尿免疫固定电泳',
		FieldType: '1',
		FieldName: 'PN'
	},
	{
		ID: 'B_PN',
		IsExportExcel: 'true',
		Display: '血免疫固定电泳',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '1'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '1'
		},
		Remark: '阴性阳性',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_BIFE',
		TableDisplay: '血免疫固定电泳',
		FieldType: '1',
		FieldName: 'B_PN'
	},
	{
		ID: 'U_ALBUMIN',
		IsExportExcel: 'true',
		Display: '白蛋白',
		Unit: '%',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0.15'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0.15'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_UPE',
		TableDisplay: '尿蛋白电泳',
		FieldType: '0',
		FieldName: 'U_ALBUMIN'
	},
	{
		ID: 'SMP',
		IsExportExcel: 'true',
		Display: '小分子蛋白',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_UPE',
		TableDisplay: '尿蛋白电泳',
		FieldType: '0',
		FieldName: 'SMP'
	},
	{
		ID: 'BMP',
		IsExportExcel: 'true',
		Display: '大分子蛋白',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_UPE',
		TableDisplay: '尿蛋白电泳',
		FieldType: '0',
		FieldName: 'BMP'
	},
	{
		ID: 'RBCN',
		IsExportExcel: 'true',
		Display: '红细胞数量',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URBCP',
		TableDisplay: '尿红细胞位相',
		FieldType: '0',
		FieldName: 'RBCN'
	},
	{
		ID: 'ARBCR',
		IsExportExcel: 'true',
		Display: '变形红细胞比率',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URBCP',
		TableDisplay: '尿红细胞位相',
		FieldType: '0',
		FieldName: 'ARBCR'
	},
	{
		ID: 'DESCRIPTION',
		IsExportExcel: 'true',
		Display: '描述',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_RENALBIOPSY',
		TableDisplay: '肾穿刺病理',
		FieldType: '2',
		FieldName: 'DESCRIPTION'
	},
	{
		ID: 'CONCLUSION',
		IsExportExcel: 'true',
		Display: '结论',
		EditShow: 'true',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_RENALBIOPSY',
		TableDisplay: '肾穿刺病理',
		FieldType: '2',
		FieldName: 'CONCLUSION'
	},
	{
		ID: 'UPRO24H',
		IsExportExcel: 'true',
		Display: '24h尿蛋白',
		Unit: 'g/d',
		EditShow: 'false',
		RangeValue: {
			Min: '0',
			Max: '0.15'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'true',
		OwnerTable: 'T_UPRO_24H',
		TableDisplay: '24h尿蛋白',
		FieldType: '0',
		FieldName: 'UPRO24H'
	},
	{
		ID: 'UNA24H',
		IsExportExcel: 'true',
		Display: '24h尿钠',
		Unit: 'mmol/d',
		EditShow: 'false',
		RangeValue: {
			Min: '0',
			Max: '0.15'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_URINE_ELECTRO',
		TableDisplay: '尿电解质',
		FieldType: '0',
		FieldName: 'UNA24H'
	},
	{
		ID: 'PLA2R',
		IsExportExcel: 'true',
		Display: '抗磷脂酶A2受体抗体',
		Unit: 'RU/ml',
		EditShow: 'false',
		RangeValue: {
			Min: '0',
			Max: '20'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '20'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_PLA2R_INFO',
		TableDisplay: '抗PLA2R抗体滴度 ',
		FieldType: '0',
		FieldName: 'PLA2R'
	},
	{
		ID: 'UPRO_RATIO',
		IsExportExcel: 'false',
		Display: '尿蛋白/肌酐',
		RankValue: '2',
		Unit: 'mg/g',
		EditShow: 'false',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_UPC_RATIO',
		TableDisplay: '尿蛋白比肌酐',
		FieldType: '0',
		FieldName: 'UPRO_RATIO'
	},
	{
		ID: 'UWPRO_RATIO',
		IsExportExcel: 'false',
		Display: '尿白蛋白/肌酐(mg/g)',
		RankValue: '0',
		Unit: 'mg/g',
		EditShow: 'false',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_UPC_RATIO',
		TableDisplay: '尿蛋白比肌酐',
		FieldType: '0',
		FieldName: 'UWPRO_RATIO'
	},
	{
		ID: 'UWPRO_RATIO_MMOL',
		IsExportExcel: 'false',
		Display: '尿白蛋白/肌酐(g/mmol)',
		RankValue: '1',
		Unit: 'g/mmol',
		EditShow: 'false',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_UPC_RATIO',
		TableDisplay: '尿蛋白比肌酐',
		FieldType: '0',
		FieldName: 'UWPRO_RATIO_MMOL'
	},
	{
		ID: 'ETC_PAPER',
		IsExportExcel: 'false',
		Display: '肾脏ECT报告',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '也即将B超上的结论录入数据库',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_RENAL_ETC',
		TableDisplay: '肾脏ECT',
		FieldType: '2',
		FieldName: 'ETC_PAPER'
	},
	{
		ID: 'CHEST_PAPER',
		IsExportExcel: 'false',
		Display: '胸片报告',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '也即将B超上的结论录入数据库',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_CHEST_XRAY',
		TableDisplay: '胸片',
		FieldType: '2',
		FieldName: 'CHEST_PAPER'
	},
	{
		ID: 'ECG_PAPER',
		IsExportExcel: 'false',
		Display: '心电图报告',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '也即将B超上的结论录入数据库',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_ECG',
		TableDisplay: '心电图',
		FieldType: '2',
		FieldName: 'ECG_PAPER'
	},
	{
		ID: 'HOSPITAL_RECORD_PAPER',
		IsExportExcel: 'false',
		Display: '出院记录报告',
		EditShow: 'true',
		RangeValue: {
			Min: '0',
			Max: '0'
		},
		WaringRangeValue: {
			Min: '0',
			Max: '0'
		},
		Remark: '出院记录报告',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_HOSPITAL_RECORD',
		TableDisplay: '出院记录报告',
		FieldType: '2',
		FieldName: 'HOSPITAL_RECORD_PAPER'
	},
	{
		ID: 'UVOL',
		IsExportExcel: 'true',
		Display: '尿量',
		Unit: 'ml',
		EditShow: 'false',
		RangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_UPRO_24H',
		TableDisplay: '24h尿蛋白',
		FieldType: '0',
		FieldName: 'UVOL'
	},
	{
		ID: 'HEART_RATE',
		IsExportExcel: 'true',
		Display: '心率',
		Unit: '次/分',
		EditShow: 'false',
		RangeValue: {
			Min: '60',
			Max: '100'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		Remark: '心率',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_HEART_RATE',
		TableDisplay: '心率',
		FieldType: '0',
		FieldName: 'HEART_RATE'
	},
	{
		ID: 'WEIGHT',
		IsExportExcel: 'true',
		Display: '体重',
		Unit: '千克',
		EditShow: 'false',
		RangeValue: {
			Min: '45',
			Max: '300'
		},
		WaringRangeValue: {
			Min: 'NA',
			Max: 'NA'
		},
		Remark: '体重',
		IsEarlyWarining: 'false',
		IsDrawChart: 'false',
		OwnerTable: 'T_WEIGHTRECORD',
		TableDisplay: '体重',
		FieldType: '0',
		FieldName: 'WEIGHT'
	},
	{
		"ID": "KAP",
		"Display": "轻链kap",
		"Unit": "mg/dl",
		"EditShow": "true",
		"RankValue": "0",
		"RangeValue": {
			"Min": "598",
			"Max": "1329"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_KLLC",
		"TableDisplay": "血清轻链",
		"FieldType": "0",
		"FieldName": "KAP"
	},
	{
		"ID": "LAM",
		"Display": "轻链lam",
		"Unit": "mg/dl",
		"EditShow": "true",
		"RankValue": "1",
		"RangeValue": {
			"Min": "280",
			"Max": "665"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_KLLC",
		"TableDisplay": "血清轻链",
		"FieldType": "0",
		"FieldName": "LAM"
	},
	{
		"ID": "C1Q_DX",
		"Display": "抗C1q抗体(定性)",
		"Unit": "",
		"EditShow": "true",
		"RankValue": "0",
		"RangeValue": {
			"Min": "0",
			"Max": "1"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_C1Q",
		"TableDisplay": "抗C1q抗体",
		"FieldType": "1",
		"FieldName": "C1Q_DX"
	},
	{
		"ID": "ZTLB_CELL",
		"Display": "总T淋巴细胞",
		"Unit": "%",
		"EditShow": "true",
		"RankValue": "0",
		"RangeValue": {
			"Min": "50",
			"Max": "82"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_LBXB",
		"TableDisplay": "外周淋巴细胞亚群",
		"FieldType": "0",
		"FieldName": "ZTLB_CELL"
	},
	{
		"ID": "ZBLB_CELL",
		"Display": "总B淋巴细胞",
		"Unit": "%",
		"EditShow": "true",
		"RankValue": "1",
		"RangeValue": {
			"Min": "5",
			"Max": "21"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_LBXB",
		"TableDisplay": "外周淋巴细胞亚群",
		"FieldType": "0",
		"FieldName": "ZBLB_CELL"
	},
	{
		"ID": "TFTLLB_CELL",
		"Display": "T辅助/调节淋巴细胞",
		"Unit": "%",
		"EditShow": "true",
		"RankValue": "2",
		"RangeValue": {
			"Min": "24",
			"Max": "54"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_LBXB",
		"TableDisplay": "外周淋巴细胞亚群",
		"FieldType": "0",
		"FieldName": "TFTLLB_CELL"
	},
	{
		"ID": "TYDLB_CELL",
		"Display": "T抑制/细胞毒淋巴细胞",
		"Unit": "%",
		"EditShow": "true",
		"RankValue": "3",
		"RangeValue": {
			"Min": "14",
			"Max": "41"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_LBXB",
		"TableDisplay": "外周淋巴细胞亚群",
		"FieldType": "0",
		"FieldName": "TYDLB_CELL"
	},
	{
		"ID": "ZNKLB_CELL",
		"Display": "总NK淋巴细胞",
		"Unit": "%",
		"EditShow": "true",
		"RankValue": "4",
		"RangeValue": {
			"Min": "6",
			"Max": "38"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_LBXB",
		"TableDisplay": "外周淋巴细胞亚群",
		"FieldType": "0",
		"FieldName": "ZNKLB_CELL"
	},
	{
		"ID": "TLBH_S_RATIO",
		"Display": "T淋巴细胞H/S比值",
		"Unit": "",
		"EditShow": "true",
		"RankValue": "5",
		"RangeValue": {
			"Min": "0.7",
			"Max": "3.1"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_LBXB",
		"TableDisplay": "外周淋巴细胞亚群",
		"FieldType": "0",
		"FieldName": "TLBH_S_RATIO"
	},
	{
		"ID": "TBNK_CELL",
		"Display": "淋巴细胞总数",
		"Unit": "%",
		"EditShow": "true",
		"RankValue": "6",
		"RangeValue": {
			"Min": "94",
			"Max": "99"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_LBXB",
		"TableDisplay": "外周淋巴细胞亚群",
		"FieldType": "0",
		"FieldName": "TBNK_CELL"
	},
	{
		"ID": "ZTLB_ABS",
		"Display": "总T淋巴细胞绝对值",
		"Unit": "个/ul",
		"EditShow": "true",
		"RankValue": "7",
		"RangeValue": {
			"Min": "723",
			"Max": "2737"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_LBXB",
		"TableDisplay": "外周淋巴细胞亚群",
		"FieldType": "0",
		"FieldName": "ZTLB_ABS"
	},
	{
		"ID": "TFTJLB_PCT",
		"Display": "T辅助/调节淋巴细胞百分比",
		"Unit": "%",
		"EditShow": "true",
		"RankValue": "8",
		"RangeValue": {
			"Min": "34",
			"Max": "70"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_LBXB",
		"TableDisplay": "外周淋巴细胞亚群",
		"FieldType": "0",
		"FieldName": "TFTJLB_PCT"
	},
	{
		"ID": "TFTJLB_ABS",
		"Display": "T辅助/调节淋巴细胞绝对值",
		"Unit": "个/ul",
		"EditShow": "true",
		"RankValue": "9",
		"RangeValue": {
			"Min": "404",
			"Max": "1612"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_LBXB",
		"TableDisplay": "外周淋巴细胞亚群",
		"FieldType": "0",
		"FieldName": "TFTJLB_ABS"
	},
	{
		"ID": "TYDLB_PCT",
		"Display": "T抑制/细胞毒淋巴细胞百分比",
		"Unit": "%",
		"EditShow": "true",
		"RankValue": "10",
		"RangeValue": {
			"Min": "14",
			"Max": "41"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_LBXB",
		"TableDisplay": "外周淋巴细胞亚群",
		"FieldType": "0",
		"FieldName": "TYDLB_PCT"
	},
	{
		"ID": "TYDLB_ABS",
		"Display": "T抑制/细胞毒淋巴细胞绝对值",
		"Unit": "个/ul",
		"EditShow": "true",
		"RankValue": "11",
		"RangeValue": {
			"Min": "220",
			"Max": "1129"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_LBXB",
		"TableDisplay": "外周淋巴细胞亚群",
		"FieldType": "0",
		"FieldName": "TYDLB_ABS"
	},
	{
		"ID": "CD4_CD8_RATIO",
		"Display": "CD4/CD8比值",
		"Unit": "",
		"EditShow": "true",
		"RankValue": "12",
		"RangeValue": {
			"Min": "0.7",
			"Max": "3.1"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_LBXB",
		"TableDisplay": "外周淋巴细胞亚群",
		"FieldType": "0",
		"FieldName": "CD4_CD8_RATIO"
	},
	{
		"ID": "ZBLB_ABS",
		"Display": "总B淋巴细胞绝对值",
		"Unit": "个/ul",
		"EditShow": "true",
		"RankValue": "13",
		"RangeValue": {
			"Min": "80",
			"Max": "616"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_LBXB",
		"TableDisplay": "外周淋巴细胞亚群",
		"FieldType": "0",
		"FieldName": "ZBLB_ABS"
	},
	{
		"ID": "ZNKLB_ABS",
		"Display": "总NK淋巴细胞绝对值",
		"Unit": "个/ul",
		"EditShow": "true",
		"RankValue": "14",
		"RangeValue": {
			"Min": "84",
			"Max": "724"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_LBXB",
		"TableDisplay": "外周淋巴细胞亚群",
		"FieldType": "0",
		"FieldName": "ZNKLB_ABS"
	},
	{
		"ID": "UV",
		"Display": "24小时尿量(UV)",
		"Unit": "ml/24h",
		"EditShow": "true",
		"RankValue": "0",
		"RangeValue": {
			"Min": "NA",
			"Max": "NA"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UV"
	},
	{
		"ID": "UCA",
		"Display": "尿钙",
		"Unit": "mmol/L",
		"EditShow": "true",
		"RankValue": "1",
		"RangeValue": {
			"Min": "NA",
			"Max": "NA"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UCA"
	},
	{
		"ID": "UP",
		"Display": "尿磷",
		"Unit": "mmol/L",
		"EditShow": "true",
		"RankValue": "2",
		"RangeValue": {
			"Min": "NA",
			"Max": "NA"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UP"
	},
	{
		"ID": "UMG",
		"Display": "尿镁",
		"Unit": "mmol/L",
		"EditShow": "true",
		"RankValue": "3",
		"RangeValue": {
			"Min": "NA",
			"Max": "NA"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UMG"
	},
	{
		"ID": "UK",
		"Display": "尿钾",
		"Unit": "mmol/L",
		"EditShow": "true",
		"RankValue": "4",
		"RangeValue": {
			"Min": "NA",
			"Max": "NA"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UK"
	},
	{
		"ID": "UNA",
		"Display": "尿钠",
		"Unit": "mmol/L",
		"EditShow": "true",
		"RankValue": "5",
		"RangeValue": {
			"Min": "NA",
			"Max": "NA"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UNA"
	},
	{
		"ID": "UCL",
		"Display": "尿氯",
		"Unit": "mmol/L",
		"EditShow": "true",
		"RankValue": "6",
		"RangeValue": {
			"Min": "NA",
			"Max": "NA"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UCL"
	},
	{
		"ID": "UUREA",
		"Display": "尿尿素",
		"Unit": "mmol/L",
		"EditShow": "true",
		"RankValue": "7",
		"RangeValue": {
			"Min": "NA",
			"Max": "NA"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UUREA"
	},
	{
		"ID": "UUA",
		"Display": "尿尿酸",
		"Unit": "umol/L",
		"EditShow": "true",
		"RankValue": "8",
		"RangeValue": {
			"Min": "NA",
			"Max": "NA"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UUA"
	},
	{
		"ID": "UCREA_ELEC",
		"Display": "尿肌酐",
		"Unit": "mmol/L",
		"EditShow": "true",
		"RankValue": "9",
		"RangeValue": {
			"Min": "NA",
			"Max": "NA"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UCREA_ELEC"
	},
	{
		"ID": "UNA24H",
		"Display": "24小时尿钠定量",
		"Unit": "mmol/24h",
		"EditShow": "true",
		"RankValue": "10",
		"RangeValue": {
			"Min": "130",
			"Max": "260"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UNA24H"
	},
	{
		"ID": "UK24H",
		"Display": "24小时尿钾定量",
		"Unit": "mmol/24h",
		"EditShow": "true",
		"RankValue": "11",
		"RangeValue": {
			"Min": "25",
			"Max": "125"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UK24H"
	},
	{
		"ID": "UCL24H",
		"Display": "24小时尿氯定量",
		"Unit": "mmol/24h",
		"EditShow": "true",
		"RankValue": "12",
		"RangeValue": {
			"Min": "110",
			"Max": "250"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UCL24H"
	},
	{
		"ID": "UCA24H",
		"Display": "24小时尿钙定量",
		"Unit": "mmol/24h",
		"EditShow": "true",
		"RankValue": "13",
		"RangeValue": {
			"Min": "2.5",
			"Max": "7.5"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UCA24H"
	},
	{
		"ID": "UP24H",
		"Display": "24小时尿磷定量",
		"Unit": "mmol/24h",
		"EditShow": "true",
		"RankValue": "14",
		"RangeValue": {
			"Min": "9.7",
			"Max": "42"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UP24H"
	},
	{
		"ID": "UMG24H",
		"Display": "24小时尿尿镁定量",
		"Unit": "mmol/24h",
		"EditShow": "true",
		"RankValue": "15",
		"RangeValue": {
			"Min": "0.98",
			"Max": "10.49"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UMG24H"
	},
	{
		"ID": "UUA24H",
		"Display": "24小时尿尿酸定量",
		"Unit": "umol/24h",
		"EditShow": "true",
		"RankValue": "16",
		"RangeValue": {
			"Min": "1487",
			"Max": "4461"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UUA24H"
	},
	{
		"ID": "UUREA24H",
		"Display": "24小时尿尿素定量",
		"Unit": "mmol/24h",
		"EditShow": "true",
		"RankValue": "17",
		"RangeValue": {
			"Min": "250",
			"Max": "570"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UUREA24H"
	},
	{
		"ID": "UCA_UCR_RATIO",
		"Display": "尿钙比尿肌酐",
		"Unit": "g/gcr",
		"EditShow": "true",
		"RankValue": "18",
		"RangeValue": {
			"Min": "NA",
			"Max": "NA"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_URINE_ELECTRO",
		"TableDisplay": "24小时尿生化",
		"FieldType": "0",
		"FieldName": "UCA_UCR_RATIO"
	},
	{
		"ID": "ACL_IGM",
		"Display": "抗心磷脂抗体IgM",
		"Unit": "PL-IgM-U/m",
		"EditShow": "true",
		"RankValue": "0",
		"RangeValue": {
			"Min": "0",
			"Max": "12"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_APS",
		"TableDisplay": "APS相关抗体",
		"FieldType": "0",
		"FieldName": "ACL_IGM"
	},
	{
		"ID": "ACL_IGG",
		"Display": "抗心磷脂抗体IgG",
		"Unit": "PL-IgG-U/m",
		"EditShow": "true",
		"RankValue": "1",
		"RangeValue": {
			"Min": "0",
			"Max": "12"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_APS",
		"TableDisplay": "APS相关抗体",
		"FieldType": "0",
		"FieldName": "ACL_IGG"
	},
	{
		"ID": "B2_GP1_IGM",
		"Display": "抗β2-糖蛋白1抗体IgM",
		"Unit": "RU/ml",
		"EditShow": "true",
		"RankValue": "2",
		"RangeValue": {
			"Min": "0",
			"Max": "20"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_APS",
		"TableDisplay": "APS相关抗体",
		"FieldType": "0",
		"FieldName": "B2_GP1_IGM"
	},
	{
		"ID": "B2_GP1_IGG",
		"Display": "抗β2-糖蛋白1抗体IgG",
		"Unit": "RU/ml",
		"EditShow": "true",
		"RankValue": "3",
		"RangeValue": {
			"Min": "0",
			"Max": "20"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_APS",
		"TableDisplay": "APS相关抗体",
		"FieldType": "0",
		"FieldName": "B2_GP1_IGG"
	},
	{
		"ID": "B_ALBUMIN_RATIO",
		"Display": "白蛋白",
		"Unit": "%",
		"EditShow": "true",
		"RankValue": "0",
		"RangeValue": {
			"Min": "60.3",
			"Max": "71.4"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_BLOOD_PRO_ELE",
		"TableDisplay": "血蛋白电泳",
		"FieldType": "0",
		"FieldName": "B_ALBUMIN_RATIO"
	},
	{
		"ID": "A1_GLOBULIN",
		"Display": "α1球蛋白",
		"Unit": "%",
		"EditShow": "true",
		"RankValue": "1",
		"RangeValue": {
			"Min": "1.4",
			"Max": "2.9"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_BLOOD_PRO_ELE",
		"TableDisplay": "血蛋白电泳",
		"FieldType": "0",
		"FieldName": "A1_GLOBULIN"
	},
	{
		"ID": "A2_GLOBULIN",
		"Display": "α2球蛋白",
		"Unit": "%",
		"EditShow": "true",
		"RankValue": "2",
		"RangeValue": {
			"Min": "7.2",
			"Max": "11.3"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_BLOOD_PRO_ELE",
		"TableDisplay": "血蛋白电泳",
		"FieldType": "0",
		"FieldName": "A2_GLOBULIN"
	},
	{
		"ID": "B_GLOBULIN",
		"Display": "β球蛋白",
		"Unit": "%",
		"EditShow": "true",
		"RankValue": "3",
		"RangeValue": {
			"Min": "8.1",
			"Max": "12.7"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_BLOOD_PRO_ELE",
		"TableDisplay": "血蛋白电泳",
		"FieldType": "0",
		"FieldName": "B_GLOBULIN"
	},
	{
		"ID": "V_GLOBULIN",
		"Display": "γ球蛋白",
		"Unit": "%",
		"EditShow": "true",
		"RankValue": "4",
		"RangeValue": {
			"Min": "8.7",
			"Max": "16"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_BLOOD_PRO_ELE",
		"TableDisplay": "血蛋白电泳",
		"FieldType": "0",
		"FieldName": "V_GLOBULIN"
	},
	{
		"ID": "ABNORMAL_ZONE",
		"Display": "是否有异常条带",
		"Unit": "",
		"EditShow": "true",
		"RankValue": "5",
		"RangeValue": {
			"Min": "NA",
			"Max": "NA"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_BLOOD_PRO_ELE",
		"TableDisplay": "血蛋白电泳",
		"FieldType": "2",
		"FieldName": "ABNORMAL_ZONE"
	},
	{
		"ID": "FH_KT_DX",
		"Display": "补体FH因子抗体(定性)",
		"Unit": "",
		"EditShow": "true",
		"RankValue": "0",
		"RangeValue": {
			"Min": "0",
			"Max": "1"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_H_FACTOR",
		"TableDisplay": "H因子",
		"FieldType": "1",
		"FieldName": "FH_KT_DX"
	},
	{
		"ID": "FH_ND",
		"Display": "补体FH因子浓度",
		"Unit": "ug/ml",
		"EditShow": "true",
		"RankValue": "1",
		"RangeValue": {
			"Min": "247",
			"Max": "1010.8"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_H_FACTOR",
		"TableDisplay": "H因子",
		"FieldType": "0",
		"FieldName": "FH_ND"
	},
	{
		"ID": "PR3_DL",
		"Display": "抗蛋白酶3(PR3)抗体定量",
		"Unit": "RU/ml",
		"EditShow": "true",
		"RankValue": "0",
		"RangeValue": {
			"Min": "0",
			"Max": "20"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_ANCA_DL",
		"TableDisplay": "血管炎筛查检测(定量)",
		"FieldType": "0",
		"FieldName": "PR3_DL"
	},
	{
		"ID": "MPO_DL",
		"Display": "抗髓过氧化物酶(MPO)抗体定量",
		"Unit": "RU/ml",
		"EditShow": "true",
		"RankValue": "1",
		"RangeValue": {
			"Min": "0",
			"Max": "20"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_ANCA_DL",
		"TableDisplay": "血管炎筛查检测(定量)",
		"FieldType": "0",
		"FieldName": "MPO_DL"
	},
	{
		"ID": "GBM_DL",
		"Display": "抗肾小球基地膜抗体(GBM)定量",
		"Unit": "RU/ml",
		"EditShow": "true",
		"RankValue": "2",
		"RangeValue": {
			"Min": "0",
			"Max": "20"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "true",
		"OwnerTable": "T_ANCA_DL",
		"TableDisplay": "血管炎筛查检测(定量)",
		"FieldType": "0",
		"FieldName": "GBM_DL"
	},
	{
		"ID": "XANCA",
		"Display": "不典型ANCA",
		"EditShow": "true",
		"RangeValue": {
			"Min": "0",
			"Max": "1"
		},
		"IsEarlyWarining": "false",
		"IsDrawChart": "false",
		"OwnerTable": "T_ANCA",
		"TableDisplay": "血管炎筛查检测(定性)",
		"FieldType": "1",
		"FieldName": "XANCA"
	},
	{
		"ID": "CANCA",
		"Display": "C-ANCA",
		"EditShow": "true",
		"RangeValue": {
			"Min": "0",
			"Max": "1"
		},
		"Remark": "阴性阳性",
		"IsEarlyWarining": "false",
		"IsDrawChart": "false",
		"OwnerTable": "T_ANCA",
		"TableDisplay": "血管炎筛查检测(定性)",
		"FieldType": "1",
		"FieldName": "CANCA"
	},
	{
		"ID": "PANCA",
		"Display": "P-ANCA",
		"EditShow": "true",
		"RangeValue": {
			"Min": "0",
			"Max": "1"
		},
		"Remark": "阴性阳性",
		"IsEarlyWarining": "false",
		"IsDrawChart": "false",
		"OwnerTable": "T_ANCA",
		"TableDisplay": "血管炎筛查检测(定性)",
		"FieldType": "1",
		"FieldName": "PANCA"
	},
	{
		"ID": "PR3",
		"Display": "PR3",
		"EditShow": "true",
		"RangeValue": {
			"Min": "0",
			"Max": "1"
		},
		"Remark": "阴性阳性",
		"IsEarlyWarining": "false",
		"IsDrawChart": "false",
		"OwnerTable": "T_ANCA",
		"TableDisplay": "血管炎筛查检测(定性)",
		"FieldType": "1",
		"FieldName": "PR3"
	},
	{
		"ID": "MPO",
		"Display": "MPO",
		"EditShow": "true",
		"RangeValue": {
			"Min": "0",
			"Max": "1"
		},
		"Remark": "阴性阳性",
		"IsEarlyWarining": "false",
		"IsDrawChart": "false",
		"OwnerTable": "T_ANCA",
		"TableDisplay": "血管炎筛查检测(定性)",
		"FieldType": "1",
		"FieldName": "MPO"
	},
	{
		"ID": "ANCA",
		"Display": "抗中性粒细胞胞浆抗体",
		"EditShow": "true",
		"RangeValue": {
			"Min": "0",
			"Max": "1"
		},
		"Remark": "阴性阳性",
		"IsEarlyWarining": "false",
		"IsDrawChart": "false",
		"OwnerTable": "T_ANCA",
		"TableDisplay": "血管炎筛查检测(定性)",
		"FieldType": "1",
		"FieldName": "ANCA"
	},
	{
		"ID": "GBM_DX",
		"Display": "GBM",
		"EditShow": "true",
		"RangeValue": {
			"Min": "0",
			"Max": "1"
		},
		"Remark": "阴性阳性",
		"IsEarlyWarining": "false",
		"IsDrawChart": "false",
		"OwnerTable": "T_ANCA",
		"TableDisplay": "血管炎筛查检测(定性)",
		"FieldType": "1",
		"FieldName": "GBM_DX"
	},
];
