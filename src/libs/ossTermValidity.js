import { HttpRequest } from '@/libs/axios.js'

import {
	Message
} from 'element-ui'
import { GetOSSPolicyAndSignForMiniApp } from '@/api/api_public';
import { stringFormater } from '@/libs/dateFormater';

const axios = new HttpRequest('')

let md5 = require("@/proving/md5.js");

export async function termValidity(file, file_type = 'image') {

	if(file === null) return Message.warning('图片信息获取失败');
	let date = (new Date()).valueOf();
	let data
	try {
		data = JSON.parse(sessionStorage.getItem('oss'))
	}catch (e) {
		data = null
	}
	if(data === null || parseInt(date / 1000) > parseInt(data.expire)) {
		let dat = await GetOSSPolicyAndSignForMiniApp()

		sessionStorage.setItem('oss', JSON.stringify(dat.data))
		if(file !== null) {
			return publicRequest(file, file_type)
				.then((result) => {
					return result.data
				})
				.catch(() => {
					return null
				});
		}
	} else {

		return publicRequest(file, file_type)
			.then((result) => {
				return result.data
			})
			.catch(() => {
				return null
			});
	}
}

function publicRequest(record, file_type) {
	let data
	try {
		data = JSON.parse(sessionStorage.getItem('oss'));
	}catch (e) {
		data = null
	}

	let item = record
	if(typeof record === 'string'){
		item = dataURLtoBlob(record);
	}
	// if(Object.prototype.toString.call(record) === '[object File]')
	let formData = new FormData();
	if(file_type === 'xslx') {
		formData.append('key', data.dir+'microservice/interactivemall/tmp/' + stringFormater(+new Date(), 'YYYY-MM-DD-HH-MM-SS') + '_' + md5.md5(record)); //签名
	} else {
		formData.append('key', data.dir + md5.md5(record)); //签名
	}
	formData.append('policy', data.policy); //签名
	formData.append('OSSAccessKeyId', data.accessid); //签名
	formData.append('Signature', data.signature); //签名
	formData.append('file', item); //签名
	// formData.append('public-read', item); //签名
	formData.append('success_action_status', 200); //签名
	// axios.interceptors.response.use(function (response) {
	//   // 对响应数据做点什么
	//   response.data = formData.get("key")

	//   console.log(response, 'response')
	//   return response
	// }, function () {
	//   // 对响应错误做点什么
	//   return null
	// });

	return axios.request({
			method: 'POST',
			url: data.host,
			data: formData,
			transformResponse: [function () {

				data = formData.get("key")
				return data;
			}],
			onUploadProgress: progressEvent => {
				// let complete = (progressEvent.loaded / progressEvent.total * 100 | 0) + '%'
				// console.log('上传 ' + complete)
			}
		}
	)
}

let dataURLtoBlob = (dataurl) => {
	let arr = dataurl.split(',');
	let mime = arr[0].match(/:(.*?);/)[1];
	let bstr = atob(arr[1]);
	let n = bstr.length;
	let u8arr = new Uint8Array(n);
	while (n --) {
		u8arr[n] = bstr.charCodeAt(n);
	}
	return new Blob([u8arr], { type: mime });
};






