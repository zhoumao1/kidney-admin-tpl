export class SysNotify {
	title = '您有一条新的消息'
	options = {
		// 文字方向
		dir: "auto",
		// 通知主体
		body: "请注意查收新的消息",
		// 不自动关闭通知
		requireInteraction: false,
		// 通知图标
		icon: ""
	};
	/**
	 *
	 * @type {Notification | null}
	 */
	notification = null
	// 是否支持
	isSupport(){
		return window.Notification
	}
	// 是否有权限进行通知
	isAuth(){
		if (Notification.permission === 'granted') {
			return true
		} else if (Notification.permission === 'default') {
			// 用户还未选择，可以询问用户是否同意发送通知
			Notification.requestPermission().then(permission => {
				if (permission === 'granted') {
					return true
				} else if (permission === 'default') {
					console.warn('用户关闭授权 未刷新页面之前 可以再次请求授权');
					return false
				} else {
					return false
				}
			});
		} else {
			return false
		}
	}

	/**
	 * 显示通知
	 * @param options { {
	 *    body?: string,  通知主题
	 *    requireInteraction?: boolean 是否自动关闭
	 *    click?: function
	 *  }|string=}
	 * @param title   {string=} 通知标题
	 */
	show(options = this.options, title = this.title){
		if(!this.isAuth())return
		if(typeof options === 'string'){
			this.options.body = options
		}else if (typeof options === 'object'){
			Object.assign(this.options, options)
		}
		this.title = title
		this.notification = new Notification(this.title, this.options); // 显示通知
		if(options.click){
			this.notification.onclick = () =>{
				options.click()
				window.focus()
				this.close()
			}
		}
	}
	close(){
		this.notification.close()
	}
}



