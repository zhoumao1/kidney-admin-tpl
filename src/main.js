import Vue from 'vue'

import '@/plugins'
import '@/icons' // icon
import '@/mixin'


import '@/styles/index.scss' // global css
import '@/styles/base.less' // global css

import App from './App'
import store from './store'
import router from './router/router-index'

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
