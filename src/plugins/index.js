import Vue from 'vue'

import ElementUI from 'element-ui'
import { baseRoute } from '@/settings';

import { Table, Modal } from 'view-design';

import Load from 'vue-blur-load'


Vue.use(ElementUI, { size: 'small', zIndex: 2000 })
Vue.component('Table', Table);
Vue.component('Modal', Modal); // 待调整为 element 的 dialog


Vue.use(Load)



function changeFavicon(src) {
	const link = document.createElement("link");
	link.href = src;
	document.head.appendChild(link);
}
changeFavicon('/'+baseRoute+'/favicon.ico')

