import store from '@/store'

var util = require('./util.js')
var md5 = require('./md5.js')
// var Secret = '83e1eab2376d4bb48bd871fc578532f7';
var Secret = process.env.VUE_APP_SECRET;
var Version = '2.0.0'

export function makeSign(object, httpMethod, token) {
  // console.log(object, httpMethod)

  var obj = {}
  if (httpMethod !== 'POST') {
    obj = deepCopy(object)
  }

  obj.VERSION_INFO = Version
  obj.TS = getCurrentDateTimeMillisecond()

  obj.TOKEN = token
  obj.SECRET = Secret

  if (httpMethod === 'POST') {
    obj.POSTDATA = JSON.stringify(object)
  }
  var mapData = getObjProps(obj)
  // console.log(mapData, 'mapData')
  // 得到签名字符串
  // console.log('sign:' + toQueryString(mapData))
  var sign = md5.md5(toQueryString(mapData))
  .toUpperCase()
  // console.log(sign)
  var filterData = filterOnlyRemainVersionAndTS(mapData)
  var queryString = toQueryString(filterData)
  // debugger
  return {
    sign: sign,
    queryString: queryString
  }
}

function deepCopy(p, c) {
  let a = c || {}
  for (var i in p) {
    if (!p.hasOwnProperty(i)) {
      continue
    }
    if (typeof p[i] === 'object') {
      a[i] = p[i].constructor === Array ? [] : {}
      deepCopy(p[i], a[i])
    } else {
      a[i] = p[i]
    }
  }

  return a
}

function getCurrentDateTimeMillisecond() {
  var timestamp = new Date().valueOf()
  return timestamp
}

function getObjProps(obj) {
  var mapdata = []
  // console.log(obj, 'obj')
  for (var p in obj) {
    if (typeof obj[p] === 'function') {
      continue
    }
    // p 为属性名称，obj[p]为对应属性的值
    var item = {}
    item.Key = p.toUpperCase()
    item.Value = obj[p]
    mapdata.push(item)
  }
  mapdata.sort((a, b) => {
    return a.Key.localeCompare(b.Key)
  })
  return mapdata
}

function toQueryString(mapData) {
  if (!mapData || mapData.length === 0) return ''
  var queryStr = '?'
  for (var item in mapData) {
    if (util.isNullOrEmpty(mapData[item].Value)) {
      continue
    }
    queryStr += util.format('{0}={1}&', mapData[item].Key, mapData[item].Value)
  }
  return queryStr.slice(0, queryStr.length - 1)
}

function filterOnlyRemainVersionAndTS(mapData) {
  var filterArray = mapData.filter(function(item) {
    if (item.Key === 'VERSION_INFO' || item.Key === 'TS') {
      return true
    }
    return false
  })
  return filterArray
}
