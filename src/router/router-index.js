import Vue from 'vue'
import Router from 'vue-router'
import { routes } from '@/router/router-list'
import { getToken, removeToken, setToken } from '@/utils/auth'
import store from '@/store'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css'
import { Message } from 'element-ui';
import getPageTitle from '@/utils/get-page-title';
import { baseRoute } from '@/settings'; // progress bar style
Vue.use(Router)
const createRouter = () => new Router({
	base: baseRoute,
	mode: 'history', // require service support
	scrollBehavior: () => ({ y: 0 }),
	routes: routes
})
console.log(routes, 'routes')
const router = createRouter()
export function resetRouter() {
	const newRouter = createRouter()
	router.matcher = newRouter.matcher // reset router
}

const LOGIN_PAGE_PATH = '/login'
const whiteList = ['/login', '/auth-redirect'] // no redirect whitelist

router.beforeEach(async (to, from, next) => {
	// debugger
	const token = getToken()
	NProgress.start()
	document.title = getPageTitle(to.meta.title)
	// console.log(token, to, from, 'token')
	if (token) {
		if (to.path === LOGIN_PAGE_PATH) {
			// 如果已登录，则重定向到主页
			next({
				path: '/',
				query: {
					redirect: to.path,
					...to.query
				}
			})
			NProgress.done() // hack: https://github.com/PanJiaChen/vue-element-admin/pull/2939
		} else {
			const hasPower = store.state.user.hasPower
			// console.log(store.getters.roles, 11)
			// debugger
			if (hasPower) {
				// console.log(to, from, '已经授权')
				next()
				NProgress.done()
			} else {
				try {
					// 授权信息
					let { roles } = await store.dispatch('authorityJudgement')
					const accessRoutes = await store.dispatch('permission/generateRoutes', roles)
					// console.log(accessRoutes, 'accessRoutes')

					// 动态添加可访问路由
					router.addRoutes(accessRoutes)
					router.options.routes = store.getters.routes
					// debugger
					next({ ...to, replace: true })
					NProgress.done()
				} catch (error) {
					// remove token and go to login page to re-login
					removeToken()
					if(error.message.includes('timeout')){
						Message.error('请求超时, 请检查网络')
					}else {
						Message.error(error.message)
					}
					next({
						path: LOGIN_PAGE_PATH,
						query: {
							redirect: to.path,
							...to.query
						}
					})
					NProgress.done()
				}
			}
		}

	} else {
		if (whiteList.includes(to.path)) {
			// 在登录白名单中，直接登录
			next()
			NProgress.done()
		} else {
			console.log(to, from)
			// 其他没有访问权限的页面被重定向到登录页面
			next({
				path: LOGIN_PAGE_PATH, // 跳转到登录页
				query: {
					redirect: to.path,
					...to.query
				}
			})
			NProgress.done()
		}
	}

})
router.afterEach(() => {
	// finish progress bar
	NProgress.done()
})

export default router
