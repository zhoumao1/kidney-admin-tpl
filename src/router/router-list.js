/* Layout */
import Main from '@/layout'
import parentView from '@/components/parent-view/index'
/* Router Modules */
// import tableRouter from './modules/table'

/**
 * 注意:子菜单只在路径子时出现。length> = 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   如果设置为true，项目将不会显示在侧边栏中(默认为假)
 * alwaysShow: true               如果设置为true，将始终显示根菜单
 *                                如果没有设置alwaysShow，当项目有多个子路径时，
 *                                它将变成嵌套模式，否则不会显示根菜单
 * redirect: noRedirect           如果设置了noRedirect将不会在面包屑中重定向
 * name:'router-name'             该名称由<keep-alive>使用(必须设置!!)
 * meta : {
    roles: ['admin','editor']    控制页面角色(您可以设置多个角色)
    title: 'title'               在侧边栏和面包屑中显示的名称(推荐设置)
    icon: 'svg-name'/'el-icon-x' 图标显示在侧边栏中
    breadcrumb: false            如果设置为false，项目将隐藏在面包屑中(默认为true)
    activeMenu: '/example/list'  如果设置路径，侧边栏将突出显示您设置的路径
  }
 */

/**
 * constantRoutes
 * 没有权限要求的基本页面
 * 所有角色都可以访问
 */
export const routes = [
	// {
	//   path: '/redirect',
	//   component: Main,
	//   hidden: true,
	//   children: [
	//     {
	//       path: '/redirect/:path(.*)',
	//       component: () => import('@/views/redirect/index')
	//     }
	//   ]
	// },
	{
		path: '/login',
		name: 'login',
		component: () => import('@/views/login/index'),
		hidden: true
	},
	{
		path: '/',
		redirect: '/home',
		hidden: true,
		component: Main
	},
	{
		path: '/home',
		name: 'home',
		redirect: '/home/search_product',
		meta: {
			icon: 'el-icon-price-tag',
			title: '这是 home 标题',
			affix: false,
		},
		component: Main,
		children: [
			{
				path: 'search_product',
				name: 'search-product',
				meta: {
					title: '这是 home 子标题',
					// breadcrumb: false,
				},
				component: () => import('@/views/home/home')
			},

		]
	},
	// {
	//   path: '/auth-redirect',
	//   component: () => import('@/views/login/auth-redirect'),
	//   hidden: true
	// },
	{
		path: '/404',
		component: () => import('@/views/error-page/404'),
		hidden: true
	},
	{
		path: '/401',
		component: () => import('@/views/error-page/401'),
		hidden: true
	}

]

/**
 * asyncRoutes
 * 需要根据用户角色动态加载的路由
 */
export const asyncRoutes = [
	// 404 page must be placed at the end !!!
	{ path: '*', redirect: '/404', hidden: true }
]

