module.exports = {
	title: '这是标题',
	baseRoute: 'kyadmin',
	/**
	 * @type {boolean} true | false
	 * @description 是否固定头部
	 */
	fixedHeader: true,

	/**
	 * @type {boolean} true | false
	 * @description 是否在侧边栏显示Logo
	 */
	sidebarLogo: true,
	/**
	 * @type {boolean}   true|false
	 * @desc       是否可以展开侧边栏
	 */
	canExpandSidebar: true,
}
