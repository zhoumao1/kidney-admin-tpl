import { getToken } from '@/utils/auth';

const getters = {
  sidebar: state => state.app.sidebar,
  canExpandSidebar: state => state.settings.canExpandSidebar,
  routes: state => state.permission.routes,
  device: state => state.app.device,
  avatar: state => state.user.avatar,
  roles: state => state.user.roles,
  token: state => state.user.token,
  authInfo: state => state.user.authInfo,
  hasPower: state => state.user.hasPower,
}
export default getters
