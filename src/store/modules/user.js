import { getAccessToken, LoginKidneyOnlineAPI } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import router, { resetRouter } from '@/router/router-index'
import store from '@/store';

const state = {
	roles: [],
	avatar: '',
	userName: '',
	// 客服ID
	userId: '',
	token: getToken(),
	// 所有的用户信息都用这个
	authInfo: {},
	hasPower: false//是否有权限
}

const mutations = {
	SET_ROLES: (state, roles) => {
		state.roles = roles
	},
	setUserId(state, id) {
		state.userId = id
	},
	M_SetAvatar(state, avatar) {
		state.avatar = avatar
	},
	M_SetToken(state, token) {
		state.token = token
		setToken(token)
	},
	// 设置权限信息(有权限后)
	M_SetAuthInfo(state, info) {
		state.authInfo = info
	},
	M_DelToken(state, token) {
		state.token = ''
		removeToken()
	},
	//获得权限
	setPower(state, hasPower) {
		state.hasPower = hasPower
	}
}

const actions = {
	// 登录
	handleLogin({ commit }, { userName, password }) {
		userName = userName.trim()
		// oauthToken({ userName, password })

		return new Promise((resolve, reject) => {
			getAccessToken({
				userName,
				password
			})
				.then(res => {
					// //debugger
					const data = res.data
					console.log('获取新的Token', res.data)
					commit('M_SetToken', data.token)
					resolve()
				})
				.catch(err => {
					reject(err)
				})
		})

	},
	// 退出登录
	handleLogOut({ state, commit, dispatch }) {
		return new Promise(resolve => {
			commit('M_DelToken')
			commit('SET_ROLES', [])
			resetRouter()
			// dispatch('tagsView/delAllViews', null, { root: true })
			resolve()
		})
	},

	//权限判断
	authorityJudgement({ state, commit, dispatch }) {
		return new Promise((resolve, reject) => {
			try {
				LoginKidneyOnlineAPI(state.token)
					.then(async ({ data }) => {
						// console.log(res, !!data.username, 'res')
						// commit('M_SetAvatar', '')
						// commit('setPower', !!data.username)
						// commit('setUserId', data['worderid'])

						commit('setUserId', data['userinfo'].userid)
						commit('M_SetAuthInfo', data)
						let flag = data['auth_info'].find(item => item.module_name === 'KYAdmin')
						commit('setPower', !!flag);

						const roles = ['admin']
						commit('SET_ROLES', roles)
						resolve({
							...data,
							roles: roles
						})
					})
					.catch(err => {
						reject(err)
					})
			} catch (error) {
				reject(error)
			}
		})
	}
}

export default {
	// namespaced: true,
	state,
	mutations,
	actions
}
