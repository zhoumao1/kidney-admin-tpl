import Cookies from 'js-cookie'
import { baseRoute } from '@/settings';

const TOKEN_KEY = baseRoute + '__token'

/**
 * 获取token
 * @returns {*} token
 */
export function getToken() {
  const token = Cookies.get(TOKEN_KEY)

  if (token) return token
  else return false
}

/**
 * 设置token
 * @param token {string}  token
 */
export function setToken(token) {
  Cookies.set(TOKEN_KEY, token, {
    expires: 7
  })
}

export function removeToken() {
  return Cookies.remove(TOKEN_KEY)
}
