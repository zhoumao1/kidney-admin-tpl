
/**
 * @param {string} url
 * @returns {Object}
 */
export function param2Obj(url) {
  const search = decodeURIComponent(url.split('?')[1]).replace(/\+/g, ' ')
  if (!search) {
    return {}
  }
  const obj = {}
  const searchArr = search.split('&')
  searchArr.forEach(v => {
    const index = v.indexOf('=')
    if (index !== -1) {
      const name = v.substring(0, index)
      const val = v.substring(index + 1, v.length)
      obj[name] = val
    }
  })
  return obj
}

/**
 * 克隆/扩展对象  用于提交数据前的赋值操作, 如果target 中有 source 的key 则进行赋值
 * @param target  {Object} 目标
 * @param source  {Object} 资源
 * @param obj     {Object} 额外资源
 */
export const cloneObjFn =(target, source, ...obj) =>{
	for (const sourceKey in source) {
		if(target.hasOwnProperty(sourceKey)){
			target[sourceKey] = source[sourceKey]
		}
		// source[sourceKey] = source[sourceKey]
	}
	Object.assign(target, ...obj)
}

/**
 * 深度合并对象, 并不是引用指针
 * @param source
 * @return {null || Object }  返回null 或者 对象 而不是数组
 */
export const deepMerge = (...source) =>{
	const result = Object.create(null)
	const deepCopy = (target) =>{

		let copyed_objs = [];//此数组解决了循环引用和相同引用的问题，它存放已经递归到的目标对象
		function _deepCopy(target){
			if((typeof target !== 'object')||!target){return target;}
			if(!['[object Object]', '[object Array]'].includes(Object.prototype.toString.call(target)))return target
			for(let i= 0 ;i<copyed_objs.length;i++){
				if(copyed_objs[i].target === target){
					return copyed_objs[i].copyTarget;
				}
			}
			let obj = {};
			if(Array.isArray(target)){
				obj = [];//处理target是数组的情况
			}
			copyed_objs.push({target:target,copyTarget:obj})
			Object.keys(target).forEach(key=>{
				if(obj[key]){ return;}
				obj[key] = _deepCopy(target[key]);
			});
			return obj;
		}
		return _deepCopy(target);
	}
	source.forEach(item =>{
		if(item){
			Object.assign(result, deepCopy(item))
		}
	})


	return result
}


/**
 * 判断一个对象是否存在key，如果传入第二个参数key，则是判断这个obj对象是否存在key这个属性
 * 如果没有传入key这个参数，则判断obj对象是否有键值对
 */
export const hasKey = (obj, key) => {
	if (key) return key in obj
	else {
		let keysArr = Object.keys(obj)
		return keysArr.length
	}
}

/**
 * @param {Array} arr1
 * @param {Array} arr2
 * @description 得到两个数组的交集, 两个数组的元素为数值或字符串
 */
export const getIntersection = (arr1, arr2) => {
	let len = Math.min(arr1.length, arr2.length)
	let i = -1
	let res = []
	while (++i < len) {
		const item = arr2[i]
		if (arr1.indexOf(item) > -1) res.push(item)
	}
	return res
}

/**
 * @param {Array} arr1
 * @param {Array} arr2
 * @description 得到两个数组的并集, 两个数组的元素为数值或字符串
 */
export const getUnion = (arr1, arr2) => {
	return Array.from(new Set([...arr1, ...arr2]))
}

export function isEmpty (str) {
	return str === null || str === undefined || str.length === 0
}
