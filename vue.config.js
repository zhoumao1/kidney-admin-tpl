'use strict'
const path = require('path')
const { title, baseRoute } = require('./src/settings.js')

function resolve(dir) {
	return path.join(__dirname, dir)
}

// If your port is set to 80,
// use administrator privileges to execute the command line.
// For example, Mac: sudo npm run
// You can change the port by the following methods:
// port = 9528 npm run dev OR npm run dev --port = 9528
const port = process.env.port || process.env.npm_config_port || 9528 // dev port
const IS_BUILD_MODE = process.env.VUE_APP_RUN_MODE.includes('build')

// All configuration item explanations can be find in https://cli.vuejs.org/config/
module.exports = {
	/**
	 * You will need to set publicPath if you plan to deploy your site under a sub path,
	 * for example GitHub Pages. If you plan to deploy your site to https://foo.github.io/bar/,
	 * then publicPath should be set to "/bar/".
	 * In most cases please use '/' !!!
	 * Detail: https://cli.vuejs.org/config/#publicpath
	 */
	publicPath: '/',
	outputDir: "dist/" + (IS_BUILD_MODE ? process.env.VUE_APP_RUN_MODE.substr(6):process.env.VUE_APP_RUN_MODE),
	assetsDir: baseRoute,
	lintOnSave: process.env.NODE_ENV === 'development',
	productionSourceMap: false,
	configureWebpack: {
		// provide the app's title in webpack's name field, so that
		// it can be accessed in index.html to inject the correct title.
		name: title,
		resolve: {
			alias: {
				'@': resolve('src'),
				'_c': resolve('src/components'),
				'_v': resolve('src/views')
			}
		}
	},
	// css:{
	// 	requireModuleExtension: true,
	// 	loaderOptions:{
	// 		less:{
	// 			lessOptions:{
	// 				javascriptEnabled: true
	// 			}
	// 		}
	// 	}
	// },
	chainWebpack(config) {
		// 它可以提高第一屏的速度，建议开启预加载
		config.plugin('preload')
		.tap(() => [
			{
				rel: 'preload',
				// to ignore runtime.js
				// https://github.com/vuejs/vue-cli/blob/dev/packages/@vue/cli-service/lib/config/app.js#L171
				fileBlacklist: [/\.map$/, /hot-update\.js$/, /runtime\..*\.js$/],
				include: 'initial'
			}
		])

		// 当有很多页面时，会产生很多无意义的请求
		config.plugins.delete('prefetch')

		// 设置svg-sprite-loader
		config.module
		.rule('svg')
		.exclude
		.add(resolve('src/icons'))
		.end()
		config.module
		.rule('icons')
		.test(/\.svg$/)
		.include
		.add(resolve('src/icons'))
		.end()
		.use('svg-sprite-loader')
		.loader('svg-sprite-loader')
		.options({
			symbolId: 'icon-[name]'
		})
		.end()

		config
		.when(process.env.NODE_ENV !== 'development',
			config => {
				config
				.plugin('ScriptExtHtmlWebpackPlugin')
				.after('html')
				.use('script-ext-html-webpack-plugin', [{
					// `runtime` must same as runtimeChunk name. default is `runtime`
					inline: /runtime\..*\.js$/
				}])
				.end()
				config
				.optimization.splitChunks({
					chunks: 'all',
					cacheGroups: {
						libs: {
							name: 'chunk-libs',
							test: /[\\/]node_modules[\\/]/,
							priority: 10,
							chunks: 'initial' // 只打包最初依赖的第三方
						},
						elementUI: {
							name: 'chunk-elementUI', // 将elementUI拆分为单个包
							priority: 20, // 重量需要比libs和app更大，否则它将被打包到libs或应用到一个单独的包中
							test: /[\\/]node_modules[\\/]_?element-ui(.*)/ // in order to adapt to cnpm
						},
						commons: {
							name: 'chunk-commons',
							test: resolve('src/components'), // 可以自定义规则
							minChunks: 3, //  最小公共数字可以自定义您的规则
							priority: 5,
							reuseExistingChunk: true
						}
					}
				})
				// https:// webpack.js.org/configuration/optimization/#optimizationruntimechunk
				config.optimization.runtimeChunk('single')
			}
		)
		if (process.env.NODE_ENV === 'development') {
			config.devtool('source-map')
			// config.devtool = 'source-map';
		}
	},
	devServer: {
		contentBase: './',
		proxy: {
			// 当你请求是以/api开头的时候，则我帮你代理访问到http://localhost:3000
			// 例如：
			// /api/users  http://localhost:3000/api/users
			// 我们真是服务器接口是没有/api的
			'/api': {    // search为转发路径
				target: process.env.VUE_API,  // 目标地址
				changeOrigin: true, //设置同源  默认false，是否需要改变原始主机头为目标URL,
				pathRewrite: { '^/api': '/api' }
			},
			'/micro': {    // search为转发路径
				// target: 'http://192.168.1.130:15000/eshop',  // 目标地址
				target: process.env.VUE_MICRO_API,  // 目标地址
				// target: 'http://192.168.1.136:15000',  // 目标地址
				changeOrigin: true, //设置同源  默认false，是否需要改变原始主机头为目标URL,
				pathRewrite: { '^/micro': '' }
			}
		}
	}
}
